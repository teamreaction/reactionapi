//Init Custom JS
(function( window ){

    'use strict';

    //Jquery
    window.$ = window.jQuery = require('jquery'); //Include Jquery on whole project

    $(document).ready(function(){

        //Set AJAX headers to include csrf token on every request
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        //Global Dependencies
        require('owl.carousel'); //Owl carousel for sliders

        //Switch Pages
        switch ($(document.body).data('page-id')) {

            case 'home' :

                require('./pages/home'); //Home page JS

                break;

            case 'profile' :

                require('./pages/profile'); //Profile page JS

                break;

            case 'course' :

                require('./pages/course'); //Course page JS

                break;

        }
    });

})( window );