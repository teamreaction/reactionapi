// #################################
// The Coding Ninja: Profile Script
// #################################

//Get links div and content div
let profileLinks = document.querySelectorAll('#profileLinks div'),
    profileForm = document.querySelectorAll('#profileForm > div');

//When user selects a prominent display the appropriate content
for (let i = 0; i < profileLinks.length; i++) {
    profileLinks[i].addEventListener('click', function () {
        profileRemoveclass();
        profileForm[i].classList.add('profileVisible');
        profileLinks[i].classList.add('selected')

        $([document.documentElement, document.body]).animate({
            scrollTop: $(profileForm[i]).offset().top
        }, 500);

    });
}

//Helper function to reset classes on each link
profileRemoveclass = () => {
    for (let j = 0; j < profileLinks.length; j++) {
        profileForm[j].classList.remove('profileVisible');
        profileLinks[j].classList.remove('selected');
    }
};

//Generate new avatar
$('#avatarGenerator').on('click', function (event) {

    event.preventDefault();

    avatarCreation();
});

//Save Avatar
$('#avatarSave').on('click', function (event) {

    event.preventDefault();

    $.ajax({
        method: 'post',
        url: '/editavatar',
        data: {avatar: $('#avatarsId_1').html(), userID: $('#user_id').val()}
    }).done(function (response) {
        $('#avatarsId_0 img').attr('src', '/storage/avatars/' + response.avatar + '?' + new Date().getTime()); //Added timestamp as parameter string to force update profile avatar
    });

});


// #################################
// The Coding Ninja: Avatar Creation
// #################################


//Avatar Color Combinations
/////////////////////////
const lightColors = ['#e9a9b5', '#99c69f', '#ffe3a2', '#bdafe4', '#898888', '#92b1ff', '#fcfcfc', '#e9a9b5', '#99c69f', '#bdafe4', '#898888', '#92b1ff', '#ffe3a2', '#bdafe4', '#898888', '#92b1ff', '#fcfcfc', '#e9a9b5', '#99c69f', '#ffe3a2', '#bdafe4', '#e9a9b5', '#99c69f', '#ffe3a2', '#898888',];

const colors = ['#c72846', '#45b545', '#fcb416', '#3d2c6f', '#222222', '#0d147c', '#ededed', '#c72846', '#45b545', '#3d2c6f', '#222222', '#0d147c', '#5b4009', '#271e4c', '#0c0c0c', '#070c3a', '#a7a6a5', '#c72846', '#45b545', '#fcb416', '#3d2c6f', '#c72846', '#45b545', '#fcb416', '#222222',];

const darkColors = ['#591428', '#215421', '#5b4009', '#271e4c', '#0c0c0c', '#070c3a', '#a7a6a5', '#fcb416', '#fcb416', '#fcb416', '#fcb416', '#fcb416', '#c72846', '#c72846', '#c72846', '#c72846', '#c72846', '#222222', '#222222', '#222222', '#222222', '#3d2c6f', '#3d2c6f', '#3d2c6f', '#3d2c6f'];

const skinColors = ['#613500', '#FFB720', '#FFC050', '#282828', '#F7CEA6', '#FFFFFF', '#FFB720',];

//Get a random Color Combination
/////////////////////////
random = (a) => {
    return Math.floor(Math.random() * a.length);
};

//Creates Avatar Object
/////////////////////////
avatarCreation = () => {

    //Get random generated variables
    let no = random(colors),
        expressionSelection = Math.floor(Math.random() * 4),
        clothVar;

    //Get avatar's parts
    let heads = document.querySelector('.avatarRecipe .head'),
        skins = document.querySelector('.avatarRecipe .skinOut'),
        stripess = document.querySelector('.avatarRecipe .stripes'),
        backgrounds = document.querySelector('.avatarRecipe .background'),
        expressions = document.querySelectorAll('.avatarRecipe .expressions >g'),
        cloth1 = document.querySelector('.avatarRecipe .cloth'),
        cloth2 = document.querySelector('.avatarRecipe .cloth2');


    heads.style.fill = colors[no]; //head
    skins.style.fill = random(skinColors); //skin color
    stripess.style.fill = darkColors[no]; //stripes
    backgrounds.style.fill = lightColors[no]; //background

    //expression
    for (let i=0;i<4;i++){
        expressions[i].style.display= 'none';
    }
    expressions[expressionSelection].style.display="block"; //expression

    //cloth
    if(no % 2 === 0){
        cloth1.style.opacity = '0';
        cloth2.style.opacity = '1';
    } else {
        cloth2.style.opacity = '0';
        cloth1.style.display = 'block';
        cloth1.style.opacity = '1';
    }

};
