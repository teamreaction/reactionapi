// #################################
// The Coding Ninja: Course Script
// #################################

// ================
// Configurations for the Carousel library - Course Page
// ================
$('.owl-carousel').owlCarousel({
    items: 3,
    autoplay: false,
    loop: false,
    center: true,
    margin: 100,
    //smartSpeed: 1500,
    stagePadding: 0,
    checkVisible: true,
    nav: false,
    dots: false,
    animateIn: true,
    responsive: {
        300:{
            items: 1,
            margin: 0,
        },
        550:{
            items:1,
        },
        768: {
            items: 2
        },
        1025: {
            items: 3
        },
        1920: {
            items: 3
        },
    },
    URLhashListener: true,
    autoplayHoverPause: false,
    startPosition: 'URLHash'
});

// setup for pagination arrows
var owl = $('.sliderWrapper');
owl.owlCarousel();
// Go to the next item
$('.owl-next').click(function () {
    owl.trigger('next.owl.carousel');
});
// Go to the previous item
$('.owl-prev').click(function () {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
});




// #################################
// The Coding Ninja: Quiz Script
// #################################


// ================
// Modal Transitions
// ================
//
var quizBox= document.getElementById('quizBox'),
    library_btns = document.querySelectorAll('.library_btn');

var main_course =  $(".main_course"),
    course_contents  = $(".course_contents"),
    footerBackground = $('#footerBackground'),
    footer =  $('footer');

function showQuizBox(){
    quizBox.style.display = "flex";
    main_course.fadeOut();
    course_contents.fadeOut();
    footerBackground.fadeOut();
    footer.fadeOut();
    setTimeout(function(){
        quizBox.style.opacity = "1";
    },150);
}

//showQuizBox();

function hideQuizBox(){
    main_course.fadeIn();
    course_contents.fadeIn();
    footerBackground.fadeIn();
    footer.fadeIn();
    quizBox.style.opacity = "0";
    setTimeout(function(){
        quizBox.style.display = "none";
    },600);
}

for (let i=0; i<library_btns.length; i++){
    library_btns[i].addEventListener('click', function(){
        showQuizBox();
    }, false);
}


var exit_btn = document.getElementById('exit_btn');

exit_btn.addEventListener('click', function(){
    hideQuizBox();
},false);

// #################################
// The Coding Ninja: Course Script
// #################################

var quizData = JSON.parse(document.getElementById('quizData').innerHTML);

var quizArray = quizData.quizzes,
    quizElements = document.getElementById('quizElements'),
    theoryTitle = document.querySelector('#quizElements h1'),
    theoryText = document.querySelector('#quizElements p'),
    quiz_dots = document.querySelectorAll('.quiz_dot'),
    quizAnswersTF = document.getElementById('quizAnswersTF'),
    quizDescriptionTF = document.getElementById('quizDescriptionTF'),
    quizAnswersMC = document.getElementById('quizAnswersMC'),
    quizDescriptionMC = document.getElementById('quizDescriptionMC');

function findActiveDot() {
    for (let i = 0; i < quiz_dots.length; i++) {
        if (quiz_dots[i].classList.contains('activeDot')) {
            return i;
        } else {
        }
    }
}

theoryOrQuiz();

//console.log(quizArray);

function theoryOrQuiz(){
    var active = findActiveDot();
    for (let i = 0; i < quizArray.length; i++) {
        //console.log(quizArray[i]) ;
        //console.log(2);
        if(quizArray[active].type_id === 0){
            makeTheory(quizArray[active]);
            return
        }

        if(quizArray[active].type_id === 1){
            makeMC(quizArray[active]);
            return
        }
        if(quizArray[active].type_id === 2){
            makeTF(quizArray[active]);
            return
        }
    }

}


function makeTheory(element) {
    quizElements.classList.add('quizTypeTheory');
    theoryTitle.innerHTML = element.title;
    theoryText.innerHTML = element.theory;

}


function makeMC(element){
    quizElements.classList.add('quizMC');
    quizDescriptionMC.innerHTML = element.description;
    makeChoices(element.answers);
}

function makeTF(element){
    quizElements.classList.add('quizTypeTF');

    quizDescriptionTF.innerHTML = element.description;
    makeChoices(element.answers);

}

function makeChoices(choices){
    for (let i = 0; i < choices.length; i++) {
        var newDiv = document.createElement("div");
        var newButton = document.createElement("button");
        quizAnswersMC.appendChild(newDiv);

        newDiv.appendChild(newButton);
        newButton.innerHTML =choices[i].answer;
    }
}