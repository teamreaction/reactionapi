// #################################
// The Coding Ninja: Homepage Script
// #################################


// ================
// Inject Dependencies
// ================
import { TweenMax } from "gsap/TweenMax";

// ================
// General Functions
// ================

const
    $id = (element)=>{
        return document.getElementById(element)},

    $queries = (elements)=>{
        return document.querySelectorAll(elements)},

    $query = (element)=>{
        return document.querySelector(element);
    };



// ================
// Variables & Constants
// ================
const formButton = $query('.formButton');
const welcome = $id('welcome');
const loginTab = $id('loginTab');
const registerTab = $id('registerTab');
const loginForm = $id('login');
const registerForm = $id('register');


// ================
// Show Form on Button Click when mobile
// ================

function getViewportWidth(){
    var viewportWidth = window.innerWidth;
    // console.log(viewportWidth);
    return viewportWidth
}
//
window.addEventListener('resize', watchClicks);
watchClicks();
function watchClicks(){

    if(welcome) {
        if (window.innerWidth > 1024) {
            welcome.classList.remove('mobileShow');
        }
        formButton.addEventListener('click', function(event){
            //event.preventDefault();
            if (window.innerWidth <= 1024){
                welcome.classList.add('mobileShow');
            }
        });
    }

}

watchTabs();
function watchTabs(){

    if(loginTab) {
        loginTab.addEventListener('click', function(event){
            //event.preventDefault();
            loginTab.classList.add('checked');
            loginForm.classList.add('visibleForm');
            registerTab.classList.remove('checked');
            registerForm.classList.remove('visibleForm');
        });
        registerTab.addEventListener('click', function(event){
            //event.preventDefault();
            loginTab.classList.remove('checked');
            loginForm.classList.remove('visibleForm');
            registerTab.classList.add('checked');
            registerForm.classList.add('visibleForm');
        });
    }

}




// configurations for the Carousel library - Course Page
$('.owl-carousel').owlCarousel({
    items: 2,
    //autoplay: true,
    loop: true ,
    center: true,
    nav: false,
    animateIn: true,
    autoplay: false,
    dots: true,
    dotsContainer: '.storyDots',
    checkVisible: true,
    slideTransition: 'cubic-bezier(.215, .61, .355, 1)',
    margin: 0,
    smartSpeed: 1500,
    stagePadding: 0,
    responsive: {
        300:{
            items: 1
        },
        700: {
            items: 1
        },
        1000: {
            items: 2
        },
        1621: {
            items: 2
        },
        1920: {
            items: 2
        },
    },
    URLhashListener: true,
    autoplayHoverPause: true,

});

//setup for pagination arrows
var owl = $('.owl-carousel');
owl.owlCarousel();
owl.on('changed.owl.carousel', function(event) {
    addClassActive:true});

$('.owl-dot').click(function () {
    $(this).addClass('active');
    owl.trigger('to.owl.carousel', [$(this).index(), 300]);
});

$(".getStarted button").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#welcome").offset().top
    }, 1600);
});

// Go to the next item
// $('.owl-next').click(function () {
//     owl.trigger('next.owl.carousel');
// })
// // Go to the previous item
// $('.owl-prev').click(function () {
//     // With optional speed parameter
//     // Parameters has to be in square bracket '[]'
//     owl.trigger('prev.owl.carousel', [300]);
// })
var parallaxBackgrounds = document.querySelectorAll('.parallaxBackground'),
    parallaxBackgrounds2 = document.querySelectorAll('.parallaxBackground2'),
    html = $('html');




html.mousemove(function (e) {


    var wx = $(window).width(),
        wy = $(window).height(),
        x = e.pageX - this.offsetLeft,
        y = e.pageY - this.offsetTop,
        newx = x - wx / 2,
        newy = y - wy / 2;

    if (y<800) {
        for (let i = 0; i < parallaxBackgrounds.length; i++) {
            let speed = $(parallaxBackgrounds[i]).attr('data-speed');
            TweenMax.to($(parallaxBackgrounds[i]), 1, {x: (1 - newx * speed), y: (1 - newy * speed)});

        }
        for (let i = 0; i < parallaxBackgrounds2.length; i++) {
            let speed = $(parallaxBackgrounds2[i]).attr('data-speed');
            TweenMax.to($(parallaxBackgrounds2[i]), 1, {x: (1 - newx * speed), y: (1 - newy * 0.001)});
        }

    }
    else{
        for (let i = 0; i < parallaxBackgrounds.length; i++) {
            // let speed = $(parallaxBackgrounds[i]).attr('data-speed');
            TweenMax.to($(parallaxBackgrounds[i]), 1, {x: (1), y: (1)});

        }
        for (let i = 0; i < parallaxBackgrounds2.length; i++) {
            // let speed = $(parallaxBackgrounds2[i]).attr('data-speed');
            TweenMax.to($(parallaxBackgrounds2[i]), 1, {x: (1), y: (1)});
        }
    }
});

