
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//Include Bootstrap
require('./bootstrap');

//Init Custom JS
(function(){

    'use strict';
    
    $(document).ready(function(){

        //Global

        //Switch Pages
        switch ($('.main-content').data('page-id')) {

            case 'dashboard' :

                //Include Highcharts on admin main page
                window.Highcharts = require('highcharts');

                //Custom Charrts
                require('./admin/charts/userChart');

                break;
        }
    });

})();


// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
