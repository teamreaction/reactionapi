//Create new input field
let inputFields = $('#form-fields');
let i = $('#form-fields input').length + 1;

$('#add-field').on('click', function() {

    $('<div class="form-group row"><div class="col-md-8">' +
        '<label class="sr-only" for="answer">Answer</label>' +
        '<div class="input-group mb-2">' +
        '<div class="input-group-prepend">' +
        '<div class="input-group-text">' + i +
        '</div></div>' +
        '<input type="text" class="form-control" id="answer" name="answer[' + i +
        ']">' +
        '</div>' +
        '</div>' +
        '<div class="col-md-4">' +
        '<button type="button" id="remove-field" class="btn btn-danger btn-block">Remove</button>' +
        '</div></div>').appendTo(inputFields);
    i++;
    return false;
});

$(inputFields).on('click', '#remove-field' , function () {

    if(i > 2) {
        $(this).parent().parent().remove();
        i--;
    }
    return false;
});

//Toggle input fields on appropriate condition
if($('#type').val() == 0){

    $('#simple_theory').show();
    $('#multiple_choice').hide();
    $('#true_false').hide();

} else if($('#type').val() == 1) {

    $('#simple_theory').hide();
    $('#multiple_choice').show();
    $('#true_false').hide();

} else if($('#type').val() == 2) {

    $('#simple_theory').hide();
    $('#multiple_choice').hide();
    $('#true_false').show();
}

$('#type').on('change', function(){

    if($('#type').val() == 0){

        $('#simple_theory').show();
        $('#multiple_choice').hide();
        $('#true_false').hide();

    } else if($('#type').val() == 1) {

        $('#simple_theory').hide();
        $('#multiple_choice').show();
        $('#true_false').hide();

    } else if($('#type').val() == 2) {

        $('#simple_theory').hide();
        $('#multiple_choice').hide();
        $('#true_false').show();
    }
});