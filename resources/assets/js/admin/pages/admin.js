//Pending Teachers Requests
function pendingteachersdata() {

    $.ajax({

        method: 'post',
        url: '/teacherpending',
        success: function (response) {
            $('#pendingRequests').replaceWith(response.content);
        },
        complete: function (response) {
            setTimeout(pendingteachersdata, 5000);
        }
    });

}

setTimeout(pendingteachersdata,5000);