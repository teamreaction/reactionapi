
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//Include Bootstrap
require('./bootstrap');

//Init Custom JS
(function(){

    'use strict';

    $(document).ready(function(){

        //Set AJAX headers to include csrf token on every request
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        //Global
        require('./modules/sb-admin');

        //Switch Pages
        switch ($('body').data('page-id')) {

            case 'admin' : //Admin front page

                require('./pages/admin');

                //Charts
                require('./charts/userChart');

                break;

            case 'chapters' : //Admin or Teacher Chapters Edit

                CKEDITOR.replace('contEditor');

                break;

            case 'courses' : //Admin or Teacher Courses Edit

                CKEDITOR.replace('contEditor');

                break;

            case 'quiz' :

                //Add Custom JS for this page
                require('./pages/quiz');

                break;
        }
    });

})();

