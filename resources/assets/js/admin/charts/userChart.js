//Content chart

$.ajax({

    method: 'post',
    url: '/currentcontent',
    success: function (response) {

        //Create chart
        const myChart = Highcharts.chart('contentChart', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Current Content'
            },
            subtitle: {
                text: 'Total Content'
            },
            xAxis: {
                categories: ['Courses', 'Chapters', 'Quizzes',]
            },
            yAxis: {
                title: {
                    text: 'Count'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Content',
                data: [response.courses, response.chapters, response.quizzes]
            }]
        });
    }
});


