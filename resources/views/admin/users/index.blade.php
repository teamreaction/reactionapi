@extends('layouts.admin', ['title' => 'Users | ' . config('app.name'), 'pageID' => 'users' ])


@section('content')

    @if(session()->get('success'))
        <div class="container">
            <div id="notification" class="alert alert-success text-center">
                {{ session()->get('success') }}
            </div>
        </div>
    @endif

    <!-- Courses table -->
    <div class="container">
        <div class="card">
            <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Users</h3>
            <div class="card-body">
                <div id="table" class="table-editable">
                    <table class="table table-bordered table-responsive-md text-center">
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">First Name</th>
                            <th class="text-center">Last Name</th>
                            <th class="text-center">Username</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Role</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Remove</th>
                        </tr>
                        @foreach($users as $user)
                            <tr>
                                <td class="pt-3-half" contenteditable="true">{{ $user['id'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $user['first_name'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $user['last_name'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $user['username'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $user['email'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $user['type'] }}</td>
                                <td>
                                    <a href="{{ route('users.edit', $user['id']) }}"
                                       class="btn btn-outline-warning btn-sm my-0">Edit</a>
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#deleteModal{{$user['id']}}"
                                       class="btn btn-outline-danger btn-sm my-0">Delete</a>
                                </td>
                            </tr>

                            {{--Include delete modal--}}
                            @include('layouts.includes.admin.modals.delete', ['route' => 'users.destroy', 'item_id' => $user['id'], 'title' => 'user'])

                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="row pagination-links">
            {{ $users->links() }}
        </div>
    </div>

@endsection