@extends('layouts.admin', ['title' => 'Edit User | ' . config('app.name'), 'pageID' => 'users' ])


@section('content')

    <div class="main-content">

        <div class="container">
            <div class="card">
                <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Edit User</h3>
                <div class="card-body">
                    <form action="{{ route('users.update', $user['id']) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name">First Name</label>
                            <input id="first_name" type="text" name="first_name" class="form-control" value="{{ $user['first_name'] }}">
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name">First Name</label>
                            <input id="last_name" type="text" name="last_name" class="form-control" value="{{ $user['last_name'] }}">
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username">First Name</label>
                            <input id="username" type="text" name="username" class="form-control" value="{{ $user['username'] }}">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">First Name</label>
                            <input id="email" type="text" name="email" class="form-control" value="{{ $user['email'] }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type">User Type</label>
                            <select id="type" name="type" class="form-control">
                                <option class="text-dark" value="{{ CodingNinja\User::DEFAULT_TYPE }}" @if($user['type'] == CodingNinja\User::DEFAULT_TYPE) selected @endif>{{ ucfirst(CodingNinja\User::DEFAULT_TYPE) }}</option>
                                <option class="text-dark" value="{{ CodingNinja\User::TEACHER_TYPE }}" @if($user['type'] == CodingNinja\User::TEACHER_TYPE) selected @endif>{{ ucfirst(CodingNinja\User::TEACHER_TYPE) }}</option>
                                <option class="text-dark" value="{{ CodingNinja\User::ADMIN_TYPE }}" @if($user['type'] == CodingNinja\User::ADMIN_TYPE) selected @endif>{{ ucfirst(CodingNinja\User::ADMIN_TYPE) }}</option>
                            </select>
                            @if ($errors->has('type'))
                                <span class="help-block">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-sm-6 offset-sm-3 col-md-4 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-action">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection