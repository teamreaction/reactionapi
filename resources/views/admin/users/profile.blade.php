@extends('layouts.admin')

@section('content')

    @if(session()->get('success'))
        <div class="row">
            <div class="col-md-8">
                <div class="alert alert-primary">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="tim-icons icon-simple-remove"></i>
                    </button>
                    <span>{{ session()->get('success') }}</span>
                </div>
            </div>
        </div>
    @endif
    
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Profile</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('editprofile') }}" method="post">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        <div class="row">
                            <div class="col-md-6 pr-md-1">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ $user['first_name'] }}">
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pl-md-1">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="last_name"  placeholder="Last Name" value="{{ $user['last_name'] }}">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 pr-md-1">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" placeholder="Username" value="{{ $user['username'] }}">
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 pl-md-1">
                                <div class="form-group">
                                    <label>Your Role</label>
                                    <input type="text" class="form-control" placeholder="Role" value="{{ $user['type'] }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $user['email'] }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-md-1">
                                <div class="form-group">
                                    <label>Facebook</label>
                                    <input type="text" class="form-control" name="facebook" placeholder="Facebook" value="{{ $user['facebook'] }}">
                                </div>
                            </div>
                            <div class="col-md-4 px-md-1">
                                <div class="form-group">
                                    <label>Instagram</label>
                                    <input type="text" class="form-control" name="instagram" placeholder="Instagram" value="{{ $user['instagram'] }}">
                                </div>
                            </div>
                            <div class="col-md-4 pl-md-1">
                                <div class="form-group">
                                    <label>Github</label>
                                    <input type="text" class="form-control" name="github" placeholder="Github" value="{{ $user['github'] }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>About Me</label>
                                    <textarea rows="4" cols="80" class="form-control" name="about_me" placeholder="Here can be your description" value="Mike">{{ $user['about_me'] }}</textarea>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-fill btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-body">
                    <p class="card-text">
                        <div class="author">
                            <div class="block block-one"></div>
                            <div class="block block-two"></div>
                            <div class="block block-three"></div>
                            <div class="block block-four"></div>
                            <a href="javascript:void(0)">
                                <img class="avatar" src="../assets/img/emilyz.jpg" alt="...">
                                <h5 class="title">{{ $user['username'] }}</h5>
                            </a>
                </div>
                </p>
                <div class="card-description">
                    {{ $user['about_me'] }}
                </div>
            </div>
            <div class="card-footer">
                <div class="button-container">
                    @if($user['facebook'])
                        <button onclick="window.open('{{ url($user['facebook']) }}')" class="btn btn-icon btn-round btn-facebook">
                            <i class="fab fa-facebook"></i>
                        </button>
                    @endif
                        @if($user['instagram'])
                        <button onclick="window.open('{{ url($user['instagram']) }}')" class="btn btn-icon btn-round btn-twitter">
                            <i class="fab fa-instagram"></i>
                        </button>
                        @endif
                            @if($user['github'])
                                <button onclick="window.open('{{ url($user['github']) }}')" class="btn btn-icon btn-round btn-twitter">
                                    <i class="fab fa-github"></i>
                                </button>
                            @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection