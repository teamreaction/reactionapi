@extends('layouts.admin', ['title' => 'Courses | ' . config('app.name'), 'pageID' => 'courses' ])


@section('content')

    @if(session()->get('success'))
        <div class="container">
            <div id="notification" class="alert alert-success text-center">
                {{ session()->get('success') }}
            </div>
        </div>
    @endif

    <!-- Courses table -->
    <div class="container">
        <div class="card">
            <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Courses</h3>
            <div class="card-body">
                <div id="table" class="table-editable">
                    <table class="table table-bordered table-responsive-md text-center">
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Title</th>
                            <th class="text-center">Slug</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Remove</th>
                        </tr>
                        @foreach($courses as $course)
                            <tr>
                                <td class="pt-3-half" contenteditable="true">{{ $course['id'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $course['title'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $course['slug'] }}</td>
                                <td>
                                    <a href="{{ route('courses.edit', $course['id']) }}"
                                       class="btn btn-outline-warning btn-sm my-0">Edit</a>
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#deleteModal{{$course['id']}}"
                                       class="btn btn-outline-danger btn-sm my-0">Delete</a>
                                </td>
                            </tr>

                            {{--Include delete modal--}}
                            @include('layouts.includes.admin.modals.delete', ['route' => 'courses.destroy', 'item_id' => $course['id'], 'title' => 'course'])

                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="row pagination-links">

        </div>
    </div>

@endsection