@extends('layouts.admin', ['title' => 'Edit Course | ' . config('app.name'), 'pageID' => 'courses' ])


@section('content')

        <div class="container">
        <div class="card">
            <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Edit Course</h3>
            <div class="card-body">
                <form action="{{ route('courses.update', $course['id']) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title">Title</label>
                        <input id="title" type="text" name="title" class="form-control" value="{{ $course['title'] }}">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="slug">Description</label>
                        <input id="slug" class="form-control rounded-0" value="{{ $course['slug'] }}" disabled>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="cont">Content</label>
                        <textarea id="contEditor" class="form-control rounded-0" name="content" rows="10">
                            {{ $course['content'] }}
                        </textarea>
                        @if ($errors->has('content'))
                            <span class="help-block">
                                <strong>{{ $errors->first('content') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-sm-6 offset-sm-3 col-md-4 offset-md-4">
                        <button class="btn btn-primary btn-action">Update</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

@endsection