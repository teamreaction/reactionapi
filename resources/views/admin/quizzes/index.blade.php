@extends('layouts.admin', ['title' => 'Quizzes | ' . config('app.name'), 'pageID' => 'quiz' ])


@section('content')

    @if(session()->get('success'))
        <div class="container">
            <div id="notification" class="alert alert-success text-center">
                {{ session()->get('success') }}
            </div>
        </div>
    @endif

    <!-- Quizzes table -->
    <div class="container">
        <div class="card">
            <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Quizzes</h3>
            <div class="card-body">
                <div id="table" class="table-editable">
                    <table class="table table-bordered table-responsive-md text-center">
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Title</th>
                            <th class="text-center">Chapter</th>
                            <th class="text-center">Type</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Remove</th>
                        </tr>
                        @foreach($quizzes as $quiz)
                            <tr>
                                <td class="pt-3-half" contenteditable="true">{{ $quiz['id'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $quiz['title'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ $quiz->chapter['title'] }}</td>
                                <td class="pt-3-half" contenteditable="true">{{ \CodingNinja\Quiz::TYPES[$quiz['type_id']] }}</td>
                                <td>
                                    <a href="{{ route('quizzes.edit', $quiz['id']) }}"
                                       class="btn btn-outline-warning btn-sm my-0">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('quizzes.destroy', $quiz['id']) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-outline-danger btn-sm my-0">Remove</button>
                                        </span>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="row pagination-links">

        </div>
    </div>

@endsection