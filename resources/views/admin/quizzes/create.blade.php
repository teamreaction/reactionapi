@extends('layouts.admin', ['title' => 'Create Quiz | ' . config('app.name'), 'pageID' => 'quiz' ])


@section('content')

    <form action="{{ route('quizzes.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="row px-3">
            <div class="col-lg-7">
                <div class="card">
                    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Create Quiz</h3>
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title">Title</label>
                            <input id="title" type="text" name="title" class="form-control" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('chapter_id') ? ' has-error' : '' }}">
                            <label for="chapter_id">Chapter</label>
                            <select id="chapter_id" name="chapter_id" class="form-control">
                                @foreach($chapters as $chapter)
                                    <option class="text-dark"
                                            value="{{ $chapter['id'] }}">{{ $chapter['title'] }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('chapter_id'))
                                <span class="help-block">
                                <strong>{{ $errors->first('chapter_id') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input type="text" id="slug" class="form-control rounded-0" name="slug"
                                   value="Slug will be added automatically" disabled>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">Description</label>
                            <textarea id="description" class="form-control rounded-0" name="description" rows="10">
                            {{ old('description') }}
                        </textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="card">
                    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Answers</h3>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select id="type" name="type_id" class="form-control">
                                @foreach(\CodingNinja\Quiz::TYPES as $key => $type)
                                    <option class="text-dark" value="{{ $key }}">{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="simple_theory" style="display: none">
                            <h5 class="text-center pb-2">Theory</h5>
                            <div class="form-group{{ $errors->has('theory') ? ' has-error' : '' }}">
                                <label for="theory">Theory</label>
                                <textarea id="theory" class="form-control rounded-0" name="theory" rows="5">
                                    {{ old('theory') }}
                                </textarea>
                                @if ($errors->has('theory'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('theory') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('example') ? ' has-error' : '' }}">
                                <label for="example">Example</label>
                                <textarea id="example" class="form-control rounded-0" name="example" rows="5">
                                    {{ old('example') }}
                                </textarea>
                                @if ($errors->has('example'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('example') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div id="multiple_choice" style="display: none">
                            <h5 class="text-center pb-2">Multiple Choice</h5>
                            <div id="form-fields">
                                <div class="form-group row">
                                    <div class="col-md-8">
                                        <label class="sr-only" for="answer">Answer</label>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">1</div>
                                            </div>
                                            <input type="text" class="form-control" id="answer" name="answer[1]">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" id="add-field" class="btn btn-info btn-block">Add</button>
                                    </div>
                                </div>
                            </div>
                            <h5 class="text-center pb-2">Correct Answer</h5>
                            <div class="form-group row">
                                <div class="col-6">
                                    <label for="correct">Select the right answer No</label>
                                </div>
                                <div class="col-6">
                                    <input type="number" id="correct" name="correct" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div id="true_false" style="display: none">
                            <h5 class="text-center pb-2">True Or False</h5>
                            <div class="form-group{{ $errors->has('correctAnswer') ? ' has-error' : '' }}">
                                <label for="correct">Check the correct answer</label>
                                <select id="correct" name="correctAnswer" class="form-control">
                                    @foreach(\CodingNinja\Answer::trueorfalse as $answer)
                                        <option class="text-dark" value="{{ $answer  }}">{{ ucfirst($answer) }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('correctAnswer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('correctAnswer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            <button type="submit" class="btn btn-dark btn-action mt-5">Create</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection