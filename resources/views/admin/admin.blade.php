@extends('layouts.admin', ['title' => 'Dashboard | ' . config('app.name'), 'pageID' => 'admin' ])

@section('content')

    <div class="container-fluid">

        <div class="row">

            @if(\Illuminate\Support\Facades\Auth::user()->type === 'admin')
                <div class="col-md-5">
                    <div class="card">
                        <h5 class="card-header text-center font-weight-bold text-uppercase py-4">Pending Teacher
                            Requests</h5>
                        <div class="card-body">
                            <div id="table" class="table-editable">
                                <table class="table table-bordered table-responsive-md text-center">
                                    <tr>
                                        <th class="text-center">Username</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                    @foreach($pendingteachers as $teacher)
                                        <tr id="pendingRequests">
                                            <td class="pt-3-half"
                                                contenteditable="false">{{ $teacher['username'] }}</td>
                                            <td class="pt-3-half" contenteditable="false">{{ $teacher['email'] }}</td>
                                            <td class="pt-3-half"
                                                contenteditable="false">{{ $teacher['verified'] }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @else

                <div class="col-md-5">
                    <h1>Welcome {{  ucfirst(\Illuminate\Support\Facades\Auth::user()['username']) }}</h1>
                    <hr>
                    <p>This is the CodingNinja Dashboard</p>
                </div>

            @endif

            <div class="col-md-7">
                <div class="card">
                    <h5 class="card-header text-center font-weight-bold text-uppercase py-4">Content Chart</h5>
                    <div class="card-body">
                        <div id="contentChart" style="width: 100%"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /.container-fluid -->

@endsection