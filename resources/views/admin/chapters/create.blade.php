@extends('layouts.admin', ['title' => 'Create Chapter | ' . config('app.name'), 'pageID' => 'chapters' ])


@section('content')

        <div class="container">
        <div class="card">
            <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Create New Chapter</h3>
            <div class="card-body">
                <form action="{{ route('chapters.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title">Title</label>
                        <input id="title" type="text" name="title" class="form-control" value="{{ old('title') }}">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
                        <label for="course_id">Course</label>
                        <select id="course_id" name="course_id" class="form-control">
                            @foreach($courses as $course)
                                <option class="text-dark" value="{{ $course['id'] }}">{{ $course['title'] }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('course_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('course_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input id="slug" class="form-control rounded-0" name="description" value="Slug will be added automatically" disabled>

                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="cont">Content</label>
                        <textarea id="contEditor" class="form-control rounded-0" name="content" rows="10">
                            {{ old('content') }}
                        </textarea>
                        @if ($errors->has('content'))
                            <span class="help-block">
                                <strong>{{ $errors->first('content') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-sm-6 offset-sm-3 col-md-4 offset-md-4">
                        <button class="btn btn-primary btn-action">Create</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

@endsection