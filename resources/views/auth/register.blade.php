{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Become a teacher | ' . config('app.name'), //page title
    'pageID' => 'teacher', //page body id for javascript
    'class' => 'teacher_registration' //page body class for css
])


@section('content')

    <div class="page_container">
        <div id="teacher_form_container">

            <form method="POST" id="teacher_form">
                {{ csrf_field() }}
                <input type="hidden" name="type" value="{{\CodingNinja\User::TEACHER_TYPE}}">

                <h2>Become a Teacher</h2>

                <input name="username" type="text" placeholder="Username" value="{{old('username')}}" required>

                @if($errors->has('username'))
                    <small>{{$errors->first('username')}}</small>
                @endif

                <input name="first_name" type="text" placeholder="Firstname" value="{{old('first_name')}}"  required>

                @if($errors->has('first_name'))
                    <small>{{$errors->first('first_name')}}</small>
                @endif

                <input name="last_name" type="text" placeholder="Lastname" value="{{old('last_name')}}" required>

                @if($errors->has('last_name'))
                    <small>{{$errors->first('last_name')}}</small>
                @endif

                <input name="password" type="password" placeholder="Password" value="{{old('password')}}" required>

                @if($errors->has('password'))
                    <small>{{$errors->first('password')}}</small>
                @endif

                <input name="password_confirmation" type="password" placeholder="Confirm Password" required>

                <input name="email" type="email" placeholder="E-mail" value="{{old('email')}}" required>

                @if($errors->has('email'))
                    <small>{{$errors->first('email')}}</small>
                @endif

                <input name="linkedin" type="text" placeholder="LinkedIn" value="{{old('linkedin')}}" required>

                @if($errors->has('linkedin'))
                    <small>{{$errors->first('linkedin')}}</small>
                @endif

                <input name="github" type="text" placeholder="Github" value="{{old('github')}}" required>

                @if($errors->has('github'))
                    <small>{{$errors->first('github')}}</small>
                @endif


                <button type="submit" class="formButton">Apply</button>
                <hr class="hr">
                <p class="terms"><a href="{{ route('privacy') }}">Privacy Policy</a> & <a href="{{ route('terms') }}">Terms of Use</a></p>
            </form>

        </div>
    </div>

@endsection