{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Login | ' . config('app.name'), //page title
    'pageID' => 'login', //page body id for javascript
    'class' => 'login_page' //page body class for css
])

@section('content')

    <div class="page_container">
        <div id="form_container">
            <form method="POST" id="login_form">
                {{csrf_field()}}

                <h2>Sign In</h2>

                @if(session()->has('info'))

                    <small>{{ session()->get('info') }}</small>

                @endif

                @if(session()->has('error'))

                    <small>{{ session()->get('error') }}</small>

                @endif

                <input name="email" type="text" placeholder="Username" id="username" value="{{old('email')}}" required autofocus>

                @if($errors->has('email'))
                    <small>{{$errors->first('email')}}</small>
                @endif

                <input name="password" type="password" placeholder="Password" id="password" value="{{old('password')}}" required>

                @if($errors->has('password'))
                    <small>{{$errors->first('password')}}</small>
                @endif

                <button type="submit" name="submit" class="formButton">Log in</button>

                <span class=""><a href="{{ route('password.request') }}">Forgot Password?</a></span>
                <hr class="hr">
                <p class="terms"><a href="{{ route('privacy') }}">Privacy Policy</a> & <a href="{{ route('terms') }}">Terms of Use</a></p>
            </form>
        </div>
    </div>

@endsection