{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Reset password | ' . config('app.name'), //page title
    'pageID' => 'forgot', //page body id for javascript
    'class' => 'forgotten_pass' //page body class for css
])

@section('content')

    <div class="page_container">
        <div id="forgotten_pass_container">
            <form method="POST" id="retrieve-pass_form" action="{{ route('password.request') }}">
                {{csrf_field()}}
                <input type="hidden" name="token" value="{{$token}}">

                <h2>Reset Password</h2>

                <p>You can choose a new password.</p>

                <input name="email" type="email" placeholder="Email" id="email" required>

                @if($errors->has('email'))
                    <small>{{$errors->first('email')}}</small>
                @endif

                <input name="password" type="password" placeholder="Password" id="password" required>

                @if($errors->has('password'))
                    <small>{{$errors->first('password')}}</small>
                @endif

                <input name="password_confirmation" type="password" placeholder="Confirm Password" id="password" required>

                @if($errors->has('password_confirmation'))
                    <small>{{$errors->first('password_confirmation')}}</small>
                @endif

                <button type="submit" name="submit" class="formButton">Apply</button>
            </form>
        </div>
    </div>

@endsection