{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Forgot your password? | ' . config('app.name'), //page title
    'pageID' => 'forgot', //page body id for javascript
    'class' => 'forgotten_pass' //page body class for css
])

@section('content')

    <div class="page_container">
        <div id="forgotten_pass_container">
            <form method="POST" id="retrieve-pass_form" action="{{ route('password.email') }}">
                {{csrf_field()}}

                <h2>Account Recovery</h2>

                @if(session('status'))

                    <small>{{ session('status') }}</small>

                @endif

                <p>Please enter your email to search for your account.</p>

                <input name="email" type="email" placeholder="Email" id="email" required>
                @if($errors->has('email'))
                    <small>{{$errors->first('email')}}</small>
                @endif

                <button type="submit" name="submit" class="formButton">Apply</button>
            </form>
        </div>
    </div>

@endsection