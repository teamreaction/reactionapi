@include('layouts.includes.admin.header', ['title' => $title, 'page-id' => $pageID])

@include('layouts.includes.admin.sidebar')

<!-- .content-wrapper -->
<div id="content-wrapper">

    @yield('content')

    <!-- Sticky Footer -->
    <footer class="sticky-footer">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright © {{ config('app.name') }} @php echo date('Y') @endphp</span>
            </div>
        </div>
    </footer>

</div>
<!-- /.content-wrapper -->

@include('layouts.includes.admin.footer')

