{{--Include header along with page variables--}}
@include('layouts.includes.frontend.header', ['title' => $title, 'page-id' => $pageID, 'class' => $class])

{{--Generate main content--}}
@yield('content')

{{--Include footer--}}
@include('layouts.includes.frontend.footer')