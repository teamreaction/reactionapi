<!-- Delete Modal-->
<div class="modal fade" id="deleteModal{{ $item_id }}" tabindex="-1" role="dialog" aria-labelledby="deleteFormModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="deleteFormModal">Danger Zone</h5>
                <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Are you sure you want to delete this {{ $title }}?</div>
            <div class="modal-footer">
                <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-outline-danger" href="{{ route($route, $item_id) }}" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">Delete</a>
                <form id="delete-form" action="{{ route($route, $item_id) }} }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </div>
        </div>
    </div>
</div>