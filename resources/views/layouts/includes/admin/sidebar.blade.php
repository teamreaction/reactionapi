<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/admin') }}">
            <i class="fa fa-fw fa-home"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-graduation-cap"></i>
            <span>Courses</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="coursesDropdown">
            <a class="dropdown-item" href="{{ route('courses.create') }}">Create New Course</a>
            <a class="dropdown-item" href="{{ url('/courses') }}">View All Courses</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-clipboard"></i>
            <span>Chapters</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="coursesDropdown">
            <a class="dropdown-item" href="{{ route('chapters.create') }}">Create New Chapter</a>
            <a class="dropdown-item" href="{{ url('/chapters') }}">View All Chapters</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-file"></i>
            <span>Quizzes</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="coursesDropdown">
            <a class="dropdown-item" href="{{ route('quizzes.create') }}">Create New Quiz</a>
            <a class="dropdown-item" href="{{ url('/quizzes') }}">View All Quizzes</a>
        </div>
    </li>
    @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/users') }}">
                <i class="fa fa-fw fa-users"></i>
                <span>Users</span></a>
        </li>
    @endif
</ul>
