</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modals-->
@include('layouts.includes.admin.modals.logout')

<!-- Custom scripts for all pages-->
<script src="{{ asset('js/admin/app.js') }}"></script>

</body>

</html>