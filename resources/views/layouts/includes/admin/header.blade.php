<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ config('app.name', 'Laravel') }} administration dashboard panel">
    <meta name="author" content="Reactive Ninjas">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600|Ubuntu+Mono" rel="stylesheet">

    <title>{{ $title }}</title>

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">

    <!-- CK editor -->
    <script src="{{ asset('js/admin/plugins/ckeditor/ckeditor.js') }}"></script>

</head>

<body id="page-top" @if(isset($pageID)) data-page-id="{{ $pageID }}" @endif>

<nav class="navbar navbar-expand bg-danger static-top">

    <a class="navbar-brand mr-1 text-white" href="{{ url('/admin') }}">
        <img src="{{ asset('images/logo.svg') }}" alt="CodingNinja" width="200" class="coding-ninja-logo">
    </a>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link text-white dropdown-toggle" href="#" id="userDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if($user = \Illuminate\Support\Facades\Auth::user())
                    @if($user->avatar['avatar'])

                        <img width="30px" src="{{ asset('storage/avatars/' . $user->avatar['avatar'] . '?' . $user->avatar['id']) }}" alt="User Avatar SVG">

                        @else

                        <img width="30px" src="{{ asset('images/avatar.svg') }}" alt="User Avatar SVG">

                    @endif
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{url('/')}}" target="_blank">Landing Page</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{route('frontend.profile')}}">Profile</a>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </div>
        </li>
    </ul>

</nav>

<div id="wrapper">
