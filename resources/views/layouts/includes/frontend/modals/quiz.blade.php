<div id='quizBox'>
    <div id="quiz">
        <div id="exit_btn"><span></span></div>

        <div id="quiz_container">
            <div class="quiz_nav">
                <div class="quiz_nav_line">
                    <div class="quiz_dot dotout "><span class="dotin"></span></div>
                    <div class="quiz_dot dotout activeDot square"><span class="dotin"></span></div>
                    <div class="quiz_dot dotout square"><span class="dotin"></span></div>
                    <div class="quiz_dot dotout "><span class="dotin"></span></div>
                    <div class="quiz_dot dotout lockedDot"><span class="dotin"></span></div>
                    <div class="quiz_dot dotout lockedDot"><span class="dotin"></span></div>
                    <div class="quiz_dot dotout lockedDot"><span class="dotin"></span></div>
                    <div class="quiz_dot dotout lockedDot challenge"><span class="dotin"></span></div>
                </div>
            </div>
            <div id="quizSection">
                <div id="quizContent">
                    <div id="quizData">

                        {
                        "course_id": 1,
                        "title": "Arrays Basics",
                        "quizzes": [{
                        "type_id": 0,
                        "title": "Arrays I",
                        "theory": "You have now mastered writting JavaScript code with primitives (strings, numbers and booleans).<br><br>But what if you have lots of data, and you want to group them in a list? Imagine you want to store things like the various products in a shopping cart, or the names of your team members, or the tasks in your to-do list.<br><br> In cases like these, Arrays come to the rescue!",
                        "example": ""
                        },
                        {
                        "type_id": 1,
                        "description": "A real life analogy for arrays is:",
                        "answers": [{
                        "answer": "A book",
                        "is_correct": 0
                        },
                        {
                        "answer": "A shopping list",
                        "is_correct": 1
                        },
                        {
                        "answer": "A chain",
                        "is_correct": 0
                        },
                        {
                        "answer": "A house",
                        "is_correct": 0
                        },
                        {
                        "answer": "Drawer",
                        "is_correct": 1
                        },
                        {
                        "answer": "All of these",
                        "is_correct": 0
                        }
                        ]
                        },
                        {
                        "type_id": 0,
                        "title": "",
                        "theory": "An array works similarly to a variable, but instead of one value, it contains multiple values. To declare it we use once again var (or let, or const). The contents of the array however, are placed inside a pair of brackets, separated with comas:",
                        "example": "var array = ['item1', 'item2', 'item3'];"
                        },
                        {
                        "type_id": 0,
                        "title": "",
                        "theory": "Inside an array we can store strings, numbers, booleans or even a combination of primitives:",
                        "example": "var array2 = ['A word', 3.45, false];"
                        },{
                        "type_id": 0,
                        "title": "",
                        "theory": "We can use an array in a similar way to variables:",
                        "example": "console.log(array2); //['A word', 3.45, false]"
                        },
                        {
                        "type_id": 2,
                        "description": "Instruction",
                        "answers": [{
                        "answer": "True",
                        "is_correct": 0
                        },
                        {
                        "answer": "False",
                        "is_correct": 1
                        }
                        ]
                        },
                        {
                        "type_id": 1,
                        "description": "Which of the following is a valid Array syntax?:",
                        "answers": [{
                        "answer": "var animals = ['dog', 'cat','goldfish'];",
                        "is_correct": 0
                        },
                        {
                        "answer": "var animals = [0,2,1];",
                        "is_correct": 0
                        },
                        {
                        "answer": "var animals = [0];",
                        "is_correct": 0
                        },
                        {
                        "answer": "var animals = [];",
                        "is_correct": 0
                        },
                        {
                        "answer": "None of them",
                        "is_correct": 0
                        },
                        {
                        "answer": "All of them",
                        "is_correct": 1
                        }
                        ]
                        },{
                        "type_id": 0,
                        "title": "",
                        "theory": "Its item inside an array has a specific position, and we can access each item using indexes. Indexes are numbers that correspond with the positions within the array. Note that indexes start with 0. For example:",
                        "example": "var myArray = [ 'first', 'second', 'third' ]; <br> console.log(myArray); //[ 'first', 'second', 'third']'"
                        },
                        {
                        "type_id": 0,
                        "title": "",
                        "theory": "In the following example that indexes always start with 0. Thus, [0] corresponds to the first item in the array.",
                        "example": "console.log(myArray[0]);  //'first'<br>console.log(myArray[1]);  //'second'<br>console.log(myArray[2]);  //'third'"
                        },
                        {
                        "type_id": 1,
                        "description": "var myPets = ['dog','cat','goldfish','hamster']<br> myPets[1] is:;",
                        "answers": [{
                        "answer": "myPets",
                        "is_correct": 0
                        },
                        {
                        "answer": "dog",
                        "is_correct": 0
                        },
                        {
                        "answer": "cat",
                        "is_correct": 1
                        },
                        {
                        "answer": "goldfish",
                        "is_correct": 0
                        },
                        {
                        "answer": "hamster",
                        "is_correct": 0
                        },
                        {
                        "answer": "bunny",
                        "is_correct": 0
                        }
                        ]
                        }
                        ]
                        }

                    </div>

                    <div id="quizElements" class=" ">
                        <div id="quizTheory">
                            <h1></h1>
                            <p class="prominent"></p>
                            <div id="quizExample"></div>
                        </div>
                        <div id="quizTF">
                            <div id="quizDescriptionTF"></div>
                            <span>Choose between True or False:</span>
                            <div id="quizAnswersTF" class="quizAnswers">
                            </div>
                        </div>
                        <div id="quizMC">
                            <div id="quizDescriptionMC"></div>
                            <span>Choose one of the following answers:</span>
                            <div id="quizAnswersMC" class="quizAnswers"></div>
                        </div>
                    </div>

                </div>
                <div id="quizButtonArea">
                    <button id="quizButton">
                        Continue
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
