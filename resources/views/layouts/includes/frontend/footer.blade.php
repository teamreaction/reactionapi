<div id="footerBackground"></div>

<footer>


    <div>
        <div class="left_foot">
            <img src='{{ asset('/images/footerLogo.svg') }}' class="footerLogo" alt="logo">
            <p>&copy; {{ now()->year }} - All rights reserved.</p>
        </div>

        <div class="right_foot">
            <div class="social">
                <a href="https://www.facebook.com/CodingNinja-981337555410790" target="_blank"><img src='{{ asset('/images/icons/facebook.svg') }}' alt="social media link"/></a>
                <a href="https://twitter.com/CodingNinja2" target="_blank"><img src='{{ asset('/images/icons/twitter.svg') }}' alt="social media link"/></a>
                <a href="https://www.instagram.com/" target="_blank"><img src='{{ asset('/images/icons/instagram.svg') }}' alt="social media link"/></a>
            </div>
            <div class="footerLinks">
                <a href="{{ route('frontend.courses') }}">COURSES</a>
                <a href="{{ route('register') }}">BECOME A TEACHER</a>
            </div>
            <div class="footerLinks">
                <a href="{{ route('contact') }}">CONTACT</a>
                <a href="{{ route('terms') }}">TERMS OF USE</a>
                <a href="{{ route('privacy') }}">PRIVACY POLICY</a>
            </div>
        </div>
    </div>

</footer>

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>

<script>
    WebFontConfig = {
        google: {
            families: ['Ubuntu Mono', 'Montserrat']
        }
    };
</script>

<!-- Bundled JS script -->
<script src="{{ asset('js/frontend/script.js') }}"></script>

</body>
</html>