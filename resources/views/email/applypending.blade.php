<!DOCTYPE html>
<html>
<head>
    <title></title>
    <!--

        An email present from your friends at Litmus (@litmusapp)

        Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.
        It's highly recommended that you test using a service like Litmus (http://litmus.com) and your own devices.

        Enjoy!

     -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
        img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
        table{border-collapse: collapse !important;}
        body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }

            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            .mobile-hide {
                display: none !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            .padding {
                padding: 10px 5% 15px 5% !important;
            }

            .padding-meta {
                padding: 30px 5% 0px 5% !important;
                text-align: center;
            }

            .padding-copy {
                padding: 10px 5% 10px 5% !important;
                text-align: center;
            }

            .no-padding {
                padding: 0 !important;
            }

            .section-padding {
                padding: 50px 15px 50px 15px !important;
            }

            /* ADJUST BUTTONS ON MOBILE */
            .mobile-button-container {
                margin: 0 auto;
                width: 100% !important;
            }

            .mobile-button {
                padding: 15px !important;
                border: 0 !important;
                font-size: 16px !important;
                display: block !important;
            }

        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] { margin: 0 !important; }
    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...
</div>

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                <tr>
                    <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                        <a href="{{ url('/') }}" target="_blank">
                            <svg width="100" height="100" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 160.97 141.7">
                                <title>Asset 3</title>
                                <g id="Layer_2" data-name="Layer 2">
                                    <g id="Homepage">
                                        <g id="Main_-_Footer" data-name="Main - Footer">
                                            <g id="Logo_Copyright" data-name="Logo &amp; Copyright">
                                                <g id="Square_Colored" data-name="Square Colored">
                                                    <g id="logo">
                                                        <g>
                                                            <path id="rightBracket" d="M107.35,95.27V84.34l44.86-23.88L107.35,36.85V25.78L161,60.52Z" style="fill: #282828"/>
                                                            <path id="leftBracket" d="M0,60.41,53.62,25.67V36.74L8.77,60.35,53.62,84.23V95.16Z" style="fill: #282828"/>
                                                            <path id="katana" d="M99.67,26.89c-.66-.32-1.61-.68-2.18-1-.33-.16-.29-.67-.17-1l.32-.83a.05.05,0,0,1,0,0v0c.42-1.09,1.07-2.76,1.12-2.91,0,0,0,0,0,0,.78-2,1.73-4.38,2.22-5.69,1.62-4.3,3.4-8.51,4.91-12.85.21-.6-.31-1.15-.83-1.5a6.24,6.24,0,0,0-2.67-1,.75.75,0,0,0-.89.52C100,4.21,98.62,8,97.19,11.68s-2.9,7.67-4.34,11.51c-.13.34-.38.74-.73.64-.61-.17-1.56-.55-2.26-.75-.25-.07-.51-.14-.64.23s-.11,1.11.13,1.19c.86.29,1.67.61,2.19.83.08,0,.52.22.55.6a2.55,2.55,0,0,1-.22,1.32c-4,10.77-8.39,21.41-12.81,32-3.57,8.59-7.36,17.08-11.21,25.54C66.21,88.42,64.5,92,62.79,95.58c-.25.52-.09.68.38.76a4.34,4.34,0,0,0,2.12-.08,1.69,1.69,0,0,0,1-1c.63-1.29,1.27-2.57,1.88-3.86C72.49,82.2,76.79,73,80.86,63.61,84.12,56.14,90,41.87,91.2,39c.07-.16.13-.32.2-.49,2-4.77,3.95-10.22,4.7-11,.3-.32.81-.08.81-.08.53.19,1.35.5,2.18.86.23.1.77-.47.9-.79S99.9,27,99.67,26.89Z" style="fill: #282828"/>
                                                            <g id="cloth">
                                                                <g id="cloth-2" data-name="cloth">
                                                                    <path d="M70.49,56.05c-4.69,6.64-12.88,6.17-20.07,5.24-6.23-.81-13.07-1.64-18.26,2.08l.38-.7c3.07-5.23,9.17-8.25,15.24-9C55.37,52.66,63.05,54.63,70.49,56.05Z" style="fill: #282828"/>
                                                                </g>
                                                                <g id="cloth-3" data-name="cloth">
                                                                    <path d="M59.51,60.07c-5.37-1.13-9.21,2.87-12.34,6.64-2.71,3.26-5.72,6.82-10,7.37.16.06.33.12.51.17,3.92,1.17,8.33-.13,11.69-2.54C53.59,68.71,56.48,64.19,59.51,60.07Z" style="fill: #282828"/>
                                                                </g>
                                                            </g>
                                                            <g>
                                                                <path d="M80.12,35.34A24.85,24.85,0,1,0,105,60.19,24.85,24.85,0,0,0,80.12,35.34ZM94.61,70.68c-4.2,1.37-10.73-.94-14.94-.94h0c-4.21,0-10.74,2.31-14.94.94-3.48-1.13-5.75-4-4.55-7.76s4.68-4,8.15-4.44a87.32,87.32,0,0,1,11.34-.74h0A87.47,87.47,0,0,1,91,58.48c3.46.46,7,.64,8.15,4.44S98.1,69.55,94.61,70.68Z" style="fill: #282828"/>
                                                                <g id="skin">
                                                                    <path d="M99.17,62.92c-1.2-3.8-4.69-4-8.15-4.44a87.47,87.47,0,0,0-11.35-.74h0a87.32,87.32,0,0,0-11.34.74c-3.47.46-6.95.64-8.15,4.44s1.07,6.63,4.55,7.76c4.2,1.37,10.73-.94,14.94-.94h0c4.21,0,10.74,2.31,14.94.94C98.1,69.55,100.36,66.69,99.17,62.92Z" style="fill: #ffe4b8"/>
                                                                    <path d="M91.43,70a38.46,38.46,0,0,1-6.2-.72,35.26,35.26,0,0,0-5.56-.65,35.27,35.27,0,0,0-5.55.65,38.46,38.46,0,0,1-6.2.72,9,9,0,0,1-2.85-.38c-2.58-.84-4.95-3-3.86-6.41.87-2.74,3.06-3.15,6.56-3.59l.7-.09a84.12,84.12,0,0,1,11.13-.73h.15a83.86,83.86,0,0,1,11.12.73l.71.09c3.5.44,5.69.85,6.55,3.59,1.1,3.45-1.27,5.57-3.85,6.41a9.11,9.11,0,0,1-2.85.38Z" style="fill: #ffecc9"/>
                                                                </g>
                                                            </g>
                                                            <path id="eye" d="M94.48,65.68c0,1.53-1.66-1.75-3.72-1.75S87,67.21,87,65.68s1.67-2.76,3.72-2.76S94.48,64.15,94.48,65.68Z" style="fill: #282828"/>
                                                            <path id="eye-2" data-name="eye" d="M72.05,65.68c0,1.53-1.67-1.75-3.72-1.75s-3.72,3.28-3.72,1.75,1.66-2.76,3.72-2.76S72.05,64.15,72.05,65.68Z" style="fill: #282828"/>
                                                        </g>
                                                    </g>
                                                    <g id="text">
                                                        <g id="paths">
                                                            <g>
                                                                <path d="M7.68,128.29c0-5.19,3.93-8.74,9-8.74a7.75,7.75,0,0,1,7.07,4.13l-3.09,1.52a4.58,4.58,0,0,0-4-2.46,5.56,5.56,0,0,0,0,11.1,4.52,4.52,0,0,0,4-2.46l3.09,1.5A7.8,7.8,0,0,1,16.68,137C11.61,137,7.68,133.49,7.68,128.29Z" style="fill: #282828"/>
                                                                <path d="M25,130.6a6.2,6.2,0,0,1,6.46-6.41,6.42,6.42,0,1,1,0,12.84A6.2,6.2,0,0,1,25,130.6Zm9.61,0c0-1.9-1.14-3.55-3.15-3.55s-3.11,1.65-3.11,3.55,1.11,3.57,3.11,3.57S34.65,132.52,34.65,130.6Z" style="fill: #282828"/>
                                                                <path d="M48.79,136.73v-1.57A4.75,4.75,0,0,1,45,137c-3.09,0-5.42-2.33-5.42-6.41s2.3-6.43,5.42-6.43a4.74,4.74,0,0,1,3.78,1.87v-6.23H52v16.9Zm0-3.95v-4.34A3.52,3.52,0,0,0,46,127.05c-1.83,0-3.09,1.44-3.09,3.57s1.26,3.55,3.09,3.55A3.52,3.52,0,0,0,48.79,132.78Z" style="fill: #282828"/>
                                                                <path d="M54.89,121.25a1.92,1.92,0,0,1,1.93-1.9,1.9,1.9,0,0,1,1.92,1.9,1.92,1.92,0,0,1-1.92,1.92A1.94,1.94,0,0,1,54.89,121.25Zm.33,15.48V124.49h3.22v12.24Z" style="fill: #282828"/>
                                                                <path d="M69.89,136.73v-7.4c0-1.7-.88-2.28-2.25-2.28a3.49,3.49,0,0,0-2.79,1.44v8.24H61.63V124.49h3.22v1.6a5.6,5.6,0,0,1,4.28-1.9c2.69,0,4,1.52,4,3.9v8.64Z" style="fill: #282828"/>
                                                                <path d="M76.05,139.87l1.45-2.33a4.85,4.85,0,0,0,3.82,1.52c1.55,0,3.37-.68,3.37-3.09v-1.24a4.77,4.77,0,0,1-3.77,1.92c-3.07,0-5.4-2.15-5.4-6.23s2.3-6.23,5.4-6.23a4.7,4.7,0,0,1,3.77,1.87v-1.57h3.25v11.4c0,4.67-3.6,5.81-6.62,5.81A7.4,7.4,0,0,1,76.05,139.87Zm8.64-7.47v-4a3.49,3.49,0,0,0-2.76-1.39,3.38,3.38,0,0,0,0,6.74A3.58,3.58,0,0,0,84.69,132.4Z" style="fill: #282828"/>
                                                                <path d="M109.35,136.73l-8.06-11v11h-3.6v-16.9h3.7l7.83,10.62V119.83h3.6v16.9Z" style="fill: #282828"/>
                                                                <path d="M115.76,121.25a1.93,1.93,0,1,1,1.92,1.92A1.94,1.94,0,0,1,115.76,121.25Zm.33,15.48V124.49h3.22v12.24Z" style="fill: #282828"/>
                                                                <path d="M130.76,136.73v-7.4a2,2,0,0,0-2.26-2.28,3.49,3.49,0,0,0-2.78,1.44v8.24H122.5V124.49h3.22v1.6a5.58,5.58,0,0,1,4.28-1.9c2.69,0,4,1.52,4,3.9v8.64Z" style="fill: #282828"/>
                                                                <path d="M133,140.84l.89-2.41a2.31,2.31,0,0,0,1.65.63c1,0,1.67-.63,1.67-2V124.49h3.22v12.57c0,2.79-1.42,4.64-4.36,4.64A4.57,4.57,0,0,1,133,140.84Zm3.88-19.59a1.92,1.92,0,0,1,1.93-1.9,1.9,1.9,0,0,1,1.92,1.9,1.92,1.92,0,0,1-1.92,1.92A1.94,1.94,0,0,1,136.84,121.25Z" style="fill: #282828"/>
                                                                <path d="M150.93,136.73v-1.27a5.06,5.06,0,0,1-3.88,1.57A4.07,4.07,0,0,1,142.8,133c0-2.86,2.3-3.95,4.25-3.95a5,5,0,0,1,3.88,1.49v-1.65c0-1.24-1.06-2.05-2.69-2.05a5.13,5.13,0,0,0-3.57,1.45l-1.22-2.16a8,8,0,0,1,5.35-1.92c2.79,0,5.35,1.11,5.35,4.63v7.91Zm0-2.94v-1.52a3.21,3.21,0,0,0-2.58-1.06c-1.27,0-2.31.65-2.31,1.85s1,1.79,2.31,1.79A3.21,3.21,0,0,0,150.93,133.79Z" style="fill: #282828"/>
                                                            </g>
                                                            <g>
                                                                <path d="M9.25,117.07v-5.25H7.36v-1.21h5.15v1.21H10.63v5.25Z" style="fill: #282828"/>
                                                                <path d="M16.54,117.07v-2.85c0-.65-.34-.85-.88-.85a1.36,1.36,0,0,0-1.06.55v3.15H13.37v-6.46H14.6V113a2.16,2.16,0,0,1,1.65-.72,1.35,1.35,0,0,1,1.52,1.47v3.32Z" style="fill: #282828"/>
                                                                <path d="M18.69,114.73a2.38,2.38,0,0,1,2.42-2.45,2.34,2.34,0,0,1,2.35,2.57v.28H20a1.26,1.26,0,0,0,1.35,1.08,2,2,0,0,0,1.25-.46l.54.79a2.89,2.89,0,0,1-1.93.65A2.37,2.37,0,0,1,18.69,114.73Zm2.42-1.48a1.13,1.13,0,0,0-1.15,1h2.31A1.09,1.09,0,0,0,21.11,113.25Z" style="fill: #282828"/>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#D8F1FF" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                <tr>
                    <td align="center" valign="top" width="500">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
                <tr>
                    <td>
                        <!-- HERO IMAGE -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="padding" align="center">
                                    <img src="{{ url('/images/journeyNinja.svg') }}" width="200" height="400" border="0" alt="Insert alt text here" style="display: block; padding: 0; color: #666666; text-decoration: none; font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <!-- COPY -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding">Welcome to {{ config('app.name') }}</td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding">Your application is now pending... <br> We will evaluate your application and will get to you as soon as possible, thank you for your patience!</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                <tr>
                    <td align="center" valign="top" width="500">
            <![endif]-->
            <!-- UNSUBSCRIBE COPY -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
                <tr>
                    <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
                        All rights reserved &copy; {{ config('app.name') }} {{ now()->year }}
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
</table>

</body>
</html>