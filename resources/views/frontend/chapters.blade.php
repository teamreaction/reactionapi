{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => $course['title'] . '| ' . config('app.name'), //page title
    'pageID' => 'course', //page body id for javascript
    'class' => 'course' //page body class for css
])

@section('content')

    <section class="main_course">
        <div class="course_title">
            <h2>{{ $course['title'] }}</h2>
        </div>

        <div class="owl-carousel sliderWrapper owl-theme">

            @foreach($chapters as $chapter)
                <div class="course_sections item " data-hash="js{{ $chapter['id'] }}">
                    <div class="section_text">
                        <h3 id="section_title">{{ $chapter['title'] }}</h3>
                        <p>
                            {!! $chapter['content'] !!}
                        </p>
                    </div>
                    <div class="section_info">
                        <div>
                            <div>Progress <span>&nbsp;@ %</span></div>
                            <div><span>@</span>/50&nbsp;<img src="{{ asset('images/icons/star.svg') }}" alt="star"/>
                            </div>
                        </div>
                        <button class="library_btn" type="button">Begin</button>
                    </div>
                </div>
            @endforeach


        </div>


        <div class="pagination_container">
            <ul class="courses_paginator owl-controls">


                <div class="owl-nav">
                    <button type="button" role="presentation" class="owl-prev disable" aria-label="Next">
                    </button>
                </div>
                @foreach($chapters as $key => $chapter)
                    <li><a href="#js{{$chapter['id']}}">{{ $key + 1 }}</a></li>
                @endforeach
                <div class="owl-controls">
                    <div class="owl-nav">
                        <button type="button" role="presentation" class="owl-next" aria-label="Previous">
                        </button>
                    </div>
                </div>
            </ul>
        </div>
    </section>

    <section class="course_contents">

        <div class="course_ninja_img">
            <object data="{{ asset('images/ninja_courses.svg') }}" type="image/svg+xml"></object>
        </div>
        <div id="courseContents">
            <div class="course_content_text">
                <h2>Courses Contents</h2>
            </div>
            <div class="course_path">

                <div class="path_1">

                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin previous"></span>
                        </div>
                        <p class="paths_text">1. Lesson</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin previous"></span>
                        </div>
                        <p class="paths_text">2. Lesson</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin current"></span>
                        </div>
                        <p class="paths_text current_text">3. Lesson</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">4. Lesson</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">5. Conditionals</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">6. Loops</p>
                    </a>

                </div>

                <div class="path_2">

                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">7. Arrays I: Basics</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">8. Arrays II: Basics</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">9. Objects 1: Basics</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">10. Lesson</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">11. Lesson</p>
                    </a>
                    <a href="#" class="contents_links">
                        <div class="dotout">
                            <span class="dotin"></span>
                        </div>
                        <p class="paths_text">12. Lesson</p>
                    </a>

                </div>
            </div>
        </div>
    </section>

    {{--Include Quiz modal--}}
    @include('layouts.includes.frontend.modals.quiz')

@endsection