{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Contact | ' . config('app.name'), //page title
    'pageID' => 'contact', //page body id for javascript
    'class' => 'contact' //page body class for css
])

{{--Main page content--}}
@section('content')

    <div style="min-height: calc(100vh - 10rem); display: flex; align-items: center; justify-content: center; ">

        <div class="contact-form">

            <h1>Contact Us</h1>

            @if(session()->has('success'))
            <p>{{ session()->get('success') }}</p>
            @endif

            <form id="contact-form" method="post" action="{{ route('email') }}">
                {{ csrf_field() }}

                <div class="email">
                    <input type="email" name="email" placeholder="Your Email*" value="{{ old('email') }}" autofocus>

                    @if ($errors->has('email'))
                        <small>{{ $errors->first('email') }}</small>
                    @endif
                </div>
                <div class="name">
                    <input type="text" placeholder="Your Name" value="{{ old('name') }}" name="name">
                </div>
                <div class="subject">
                    <input type="text" name="subject" placeholder="Subject*" value="{{ old('subject') }}">

                    @if ($errors->has('subject'))
                        <small>{{ $errors->first('subject') }}</small>
                    @endif
                </div>
                <div class="content">
                    <textarea id="message" name="message" placeholder="Your Message*" rows="8">{{ old('message') }}</textarea>

                    @if ($errors->has('message'))
                        <small>{{ $errors->first('message') }}</small>
                    @endif
                </div>

                <button type="submit">Commit</button>

            </form>

        </div>

    </div>

@endsection