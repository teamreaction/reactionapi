{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Profile | ' . config('app.name'), //page title
    'pageID' => 'profile', //page body id for javascript
    'class' => 'profile' //page body class for css
])

@section('content')

    <div id="profileContainer">
        <div id="profileDetails">
            <div id="profileAvatar">

                <div id="avatarsId_0">

                    @if($user->avatar['avatar'])

                        <img width="50%" src="{{ asset('storage/avatars/' . $user->avatar['avatar'] . '?' . $user->avatar['id']) }}" alt="User Avatar SVG">

                    @else

                        <img width="50%" src="{{ asset('images/avatar.svg') }}" alt="User Avatar SVG">

                    @endif

                </div>

                <p class="prominent">{{ $user['username'] }}</p>
            </div>
            <div id="profileLinks">
                <div class="selected prominent">PROFILE INFORMATION</div>
                <div class="prominent">CHANGE PASSWORD</div>
                <div class="prominent">CHANGE AVATAR</div>
                <div class="prominent">CHANGE EMAIL</div>
            </div>
        </div>


        <div id="profileForm">
            <div id="profileInfo" class="profileVisible">
                <h4>Your Username</h4>
                <div><input name="username" type="text" value="{{ $user['username'] }}" disabled></div>
                <h4>Your Email Address</h4>

                <div><input name="email" type="email" value="{{ $user['email'] }}" disabled></div>

                <input type="hidden" id="user_id" value="{{ $user['id'] }}">

                @if(session()->has('success'))
                    <div><small> {{ session()->get('success') }}</small></div>
                @endif

                @if(session()->has('fail'))
                    <div><small> {{ session()->get('fail') }}</small></div>
                @endif

                <button type="button" id="signOut" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign Out</button>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </div>

            <div id="changePassword">
                <form method="post" action="{{ route('frontend.editpassword') }}">
                    {{ csrf_field() }}
                    <div>
                        <p>Enter your Old Password:</p>
                        <input name="oldpassword" id="oldPassword" type="password" value="password12345">
                        <p>Enter your New Password:</p>
                        <input name="newpassword" id="newPassword" type="password">
                    </div>
                    <button type="submit">Update</button>
                </form>
            </div>


            <div id="changeAvatar">

                <div class="avatarRecipe" id="avatarsId_1">

                    @if($user->avatar['avatar'])

                        @php include './storage/avatars/' . $user->avatar['avatar'] @endphp

                        @else

                            @php include './images/avatar.svg' @endphp

                    @endif

                </div>

                <button id="avatarGenerator">Avatar Generation!</button>
                <button id="avatarSave" type="button">Save</button>

            </div>

            <div id="changeMail">
                <div>
                    <h4>Old Email Address</h4>
                    <input name="email" id="oldEmail" type="email" value="{{ $user['email'] }}" disabled></div>
                <div>
                    <h4>New Email Address</h4>
                    <form method="post" action="{{ route('frontend.editemail') }}">
                        {{ csrf_field() }}
                        <input name="email" id="newEmail" type="email"></div>
                        <button type="submit">Update</button>
                    </form>
            </div>
        </div>

    </div>


@endsection