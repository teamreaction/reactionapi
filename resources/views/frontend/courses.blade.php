{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Courses | ' . config('app.name'), //page title
    'pageID' => 'library', //page body id for javascript
    'class' => 'library' //page body class for css
])

@section('content')

    <div class="library-container">

        <section class="main_course_first">

            <div class="courses_header_a">
                <h1>web<br/>development<br/>basics</h1>
            </div>

            <div class="courses_box1_a">

                @foreach($courses as $course)

                <div class="content_box_wrapper {{ $course->checkStatus() }}">
                    <a href="{{ route('frontend.courses.chapters', ['course' => $course['slug']])}}">
                        <div class="course_title_a">
                            <h3>{{ $course['title'] }}</h3>
                        </div>

                        <div class="course_text_a">
                            <p class="prominent"> {!! $course['content'] !!} </p>
                            <div class="course_info_a">
                                <p class="prominent">Progress <span>%%</span></p>
                                <p class="prominent"><span>200</span><span>/200 </span>
                            </div>
                        </div>
                    </a>
                </div>

                @endforeach


            </div>


        </section>


@endsection