{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Home | ' . config('app.name'), //page title
    'pageID' => 'home', //page body id for javascript
    'class' => 'home' //page body class for css
])

{{--Main page content--}}
@section('content')

    <div class="sections">
        <section id="welcome" class="prominent">
            <div>
                <div id="stars" class="parallaxBackground" data-speed=".008"></div>
                <div id="moon" class="parallaxBackground" data-speed="0.02"></div>
                <div id="clouds" class="parallaxBackground" data-speed="0.04"></div>
                <div id="land" style="transform: scale(1.25)" class="parallaxBackground" data-speed="0.06"></div>
                <div id="ninjaPillar" class="parallaxBackground2" data-speed="0.09"></div>
            </div>
            <div id="welcomeContainer">


                <div id="welcomeText">
                    @if(\Illuminate\Support\Facades\Auth::check())

                        <h2>Welcome</h2>
                        <h1>{{ \Illuminate\Support\Facades\Auth::user()->username }}</h1>
                        <p>Your coding adventure began</p>

                        @else

                            <h2>Become a true</h2>
                            <h1>Coding Ninja</h1>
                            <h2>Join for free</h2>

                    @endif
                </div>


                <div class="welcomeRegister">
                    <div id="entranceForms" @if(\Illuminate\Support\Facades\Auth::check()) class="hidden" @endif>
                        <div id="formTabs">
                            <div id="registerTab" class="checked">Register</div>
                            <div id="loginTab">Sign In</div>
                        </div>
                        <form id="register" class="visibleForm" method="post" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="type" value="{{ \CodingNinja\User::DEFAULT_TYPE }}">
                            <img src="{{ asset('images/icons/ninja.svg') }}" alt="ninja"/>
                            <input name="username" type="text" placeholder="Username" value="{{ old('username') }}"
                                   required>
                                @if($errors->has('username'))
                                    <small>{{ $errors->first('username') }}</small>
                                @endif
                            <img src="{{ asset('images/icons/key.svg') }}" alt="key"/>
                            <input name="password" type="password" placeholder="Password" value="{{ old('password') }}"
                                   required>
                                @if($errors->has('password'))
                                    <small>{{ $errors->first('password') }}</small>
                                @endif
                            <img src="{{ asset('images/icons/envelope.svg') }}" alt="envelope"/>
                            <input name="email" type="email" placeholder="E-mail" value="{{ old('email') }}" required>
                                @if($errors->has('email'))
                                    <small>{{ $errors->first('email') }}</small>
                                @endif
                            <button type="submit" class="formButton">START CODING NOW</button>
                            <span class=""><a href="{{ route('register') }}">Register as a Teacher?</a></span>
                            <hr class="hr">
                            <p class="terms">
                                <a href="{{ route('privacy') }}">Privacy Policy</a> & <a href="{{ route('terms') }}">Terms of Service</a>
                            </p>
                        </form>
                        <form id="login" method="post" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <img src="{{ asset('images/icons/ninja.svg') }}" alt="ninja"/>
                            <input name="email" type="text" placeholder="Username" required>
                                @if($errors->has('email'))
                                    <small>{{ $errors->first('email') }}</small>
                                @endif
                            <img src="{{ asset('images/icons/key.svg') }}" alt="key"/>
                            <input name="password" type="password" placeholder="Password" required>
                                @if($errors->has('password'))
                                    <small>{{ $errors->first('password') }}</small>
                                @endif
                            <button type="submit" class="formButton">START CODING NOW</button>
                            <span class=""><a href="{{ route('password.request') }}">Forgot Password?</a></span>
                            <hr class="hr">
                            <p class="terms">
                                <a href="{{ route('privacy') }}">Privacy Policy</a> & <a href="{{ route('terms') }}">Terms of Service</a>
                            </p>
                        </form>


                    </div>

                </div>
            </div>
            @svg('images/welcomeFront.svg')

        </section>


        <section id="howItWorks">
            <img src="{{ asset('images/howItWorks.png') }}" alt="How It Works gif">
            <div>
                <h3>
                    if you know how to <strong>read</strong> you can learn
                    how to <strong>code</strong>
                </h3>
                <p class="prominent">
                    Enjoy online coding with easy-to-follow instructions, immediate feedback, and a tested
                    curriculum.<br><br>
                    No matter your skill level. At your own pace. Whether you are on the go, at the office, or at
                    home.<br><br>
                    Now anyone gets to say “I can code”!
                </p>
            </div>
        </section>
        <section id="journey">
            <div id="journeyBox">
                <div id="journeyTitle">
                    <h3>your coding <strong>journey</strong></h3>
                    <div id="expansion"></div>
                </div>
                <div id="journeyText">
                    <p class="prominent">A true Coding Ninja has three important values at heart helping him lead
                        through
                        the overwhelming and complex path to coding mastery. And this is exactly our philosophy here at
                        Coding Ninja.</p>
                    <img src="{{ asset('images/theory.svg') }}" alt="Theory, Practice and Revision"/>
                    <p class="prominent">The objective of each course is to learn the theory behind a particular topic
                        through a series of quizes and challenges.</br></br>
                        Our course material is focused, for now, on various Web Development principles, languages, and
                        techniques, including HTML, CSS, JavaScript, PHP, and SVG animation. Feel free to explore our
                        course
                        library.</p>
                    <button onclick="location.href='{{ route('frontend.courses') }}'">COURSES LIBRARY</button>
                </div>
            </div>
            @svg('images/journeyNinja.svg')

        </section>

        <section id="ninjaStories">
            <h3><strong>your</strong> ninja stories</h3>
            <div class="owl-carousel owl-theme owl-loaded">
                <div class="owl-stage-outer">
                    <div class="owl-stage">
                        <div class="owl-item ninjaStoryBox" id="firstStoryteller">

                            <div class="storyteller">
                                <img src="{{ asset('images/avatar5.svg') }}" alt="user's Avatar">

                                <div class="storytellerInfo">
                                    <h3>Fistikaki</h3>
                                    <div class="storyTellerStats">

                                        <span class="ninjaRank">5<sup>th</sup></span>
                                        <span class="ninjaPoints"> 425<span class="ninjaStar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <p class="story prominent">I just created my very first website. All thanks to the Coding
                                Ninja!!! It’s a great platform. Not just because I’m learning lots of amazing things
                                but because it’s quite fun as well! Can’t wait for more courses!</p>
                        </div>
                        <div class="owl-item ninjaStoryBox">

                            <div class="storyteller">
                                <img src="{{ asset('images/avatar2.svg') }}" alt="User's Avatar">

                                <div class="storytellerInfo">
                                    <h3>George92</h3>
                                    <div class="storyTellerStats">

                                        <span class="ninjaRank">17<sup>th</sup></span>
                                        <span class="ninjaPoints"> 225<span class="ninjaStar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <p class="story prominent">An absolutely great experience!
                                Already learned a lot, and many aspects of JavaScript have now been clarified.
                                Thank you Coding Ninja masters!</p>
                        </div>
                        <div class="owl-item ninjaStoryBox">

                            <div class="storyteller">
                                <img src="{{ asset('images/avatar3.svg') }}" alt="User's Avatar">

                                <div class="storytellerInfo">
                                    <h3></h3>
                                    <div class="storyTellerStats">

                                        <span class="ninjaRank">21<sup>th</sup></span>
                                        <span class="ninjaPoints"> 205<span class="ninjaStar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <p class="story prominent">An absolutely great experience!
                                Already learned a lot, and many aspects of JavaScript have now been clarified.
                                Thank you Coding Ninja masters!</p>
                        </div>
                        <div class="owl-item ninjaStoryBox">

                            <div class="storyteller">
                                <img src="{{ asset('images/avatar4.svg') }}" alt="User's Avatar">

                                <div class="storytellerInfo">
                                    <h3>Maria-san</h3>
                                    <div class="storyTellerStats">
                                        <span class="ninjaRank">4<sup>th</sup></span>
                                        <span class="ninjaPoints"> 525<span class="ninjaStar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <p class="story prominent">An absolutely great experience!
                                Already learned a lot, and many aspects of JavaScript have now been clarified.
                                Thank you Coding Ninja masters!</p>
                        </div>
                        <div class="owl-item ninjaStoryBox" id="firstStoryteller">

                            <div class="storyteller">
                                <img src="{{ 'images/avatar1.svg' }}" alt="user's Avatar">

                                <div class="storytellerInfo">
                                    <h3>George92</h3>
                                    <div class="storyTellerStats">

                                        <span class="ninjaRank">1<sup>th</sup></span>
                                        <span class="ninjaPoints"> 425<span class="ninjaStar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <p class="story prominent">I just created my very first website. All thanks to the Coding
                                Ninja!!! It’s a great platform. Not just because I’m learning lots of amazing things
                                but because it’s quite fun as well! Can’t wait for more courses!</p>
                        </div>
                    </div>
                </div>
                <div class="owl-dots storyDots">
                    <div class="owl-dot"><span></span></div>
                    <div class="owl-dot"><span></span></div>
                    <div class="owl-dot"><span></span></div>
                    <div class="owl-dot"><span></span></div>
                    <div class="owl-dot"><span></span></div>
                </div>
            </div>


        </section>

        <section id="firstFour">

            <h3 id="firstFourTitle">and that's <strong>us</strong>,<br>
                the first four <strong>coding ninjas</strong>
            </h3>

            <div class="webdevs_box">
                <div class="dev_thumbnail">
                    <img src="{{ asset('images/photos/petros.png') }}" alt="Petros Antoniou">
                    <p><strong>Petros<br>Antoniou</strong></p>
                    <p>Web Designer;}</p>
                </div>
                <div class="dev_thumbnail">
                    <img src="{{ asset('images/photos/manos.png') }}" alt="Emmanuel Vlachakis">
                    <p><strong>Emmanuel<br>Vlachakis</strong></p>
                    <p>Front-End Developer();</p>
                </div>
                <div class="dev_thumbnail">
                    <img src="{{ asset('images/photos/meletis.png') }}" alt="Meletis Nikolaidis">
                    <p><strong>Meletis<br>Nikolaidis</strong></p>
                    <p>Back-End Developer?></p>
                </div>
                <div class="dev_thumbnail">
                    <img src="{{ asset('images/photos/iosif.png') }}" alt="Iosif Gemenitzoglou">
                    <p><strong>Iosif<br> Gemenitzoglou</strong></p>
                    <p>Full-Stack Developer/></p>
                </div>
            </div>
            <div id="becomeTeacher">
                <p class="prominent">Are you a seasoned coder?<br>
                    Become a teacher and create your very own quizes and lessons!</p>
                <button type="button" id="teacherButton" onclick="location.href='/register'">BECOME A TEACHER</button>
            </div>
        </section>

        <section class="getStarted">
            <div>
                <h3>let the <strong>adventure</strong><br>commence...</h3>
                <button><a href="#welcome">GET STARTED</a></button>
            </div>
        </section>
    </div>

@endsection
