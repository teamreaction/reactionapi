{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Home | ' . config('app.name'), //page title
    'pageID' => 'home', //page body id for javascript
    'class' => 'profile' //page body class for css
])

@section('content')

    <div style="width: 70%; padding:15rem; margin: 0 auto;">
        <div style="width: 100%; background: linear-gradient(#bdafe8
, transparent 60%, rgba(255, 207, 96, .4) 90%); padding: 1rem 2rem">
            <h2 style="margin: 1rem 0rem; text-align: center">Cookie Policy for Coding Ninja</h2>

            <p>This is the Cookie Policy for Coding Ninja, accessible from https:://codingninja.eu</p><br>

            <p><strong>What Are Cookies</strong></p><br>

            <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files
                that are downloaded to your computer, to improve your experience. This page describes what information
                they gather, how we use it and why we sometimes need to store these cookies. We will also share how you
                can prevent these cookies from being stored however this may downgrade or 'break' certain elements of
                the sites functionality.</p><br>

            <p>For more general information on cookies see the Wikipedia article on HTTP Cookies.</p><br>

            <p><strong>How We Use Cookies</strong></p><br>

            <p>We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry
                standard options for disabling cookies without completely disabling the functionality and features they
                add to this site. It is recommended that you leave on all cookies if you are not sure whether you need
                them or not in case they are used to provide a service that you use.</p><br>

            <p><strong>Disabling Cookies</strong></p><br>

            <p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help
                for how to do this). Be aware that disabling cookies will affect the functionality of this and many
                other websites that you visit. Disabling cookies will usually result in also disabling certain
                functionality and features of the this site. Therefore it is recommended that you do not disable
                cookies.</p><br>

            <p><strong>The Cookies We Set</strong></p><br>

            <ul>

                <li>
                    <p>Forms related cookies</p>
                    <p>When you submit data to through a form such as those found on contact pages or comment forms
                        cookies may be set to remember your user details for future correspondence.</p><br>
                </li>

                <li>
                    <p>Site preferences cookies</p>
                    <p>In order to provide you with a great experience on this site we provide the functionality to set
                        your preferences for how this site runs when you use it. In order to remember your preferences
                        we need to set cookies so that this information can be called whenever you interact with a page
                        is affected by your preferences.</p><br>
                </li>

            </ul>

            <p><strong>Third Party Cookies</strong></p><br>

            <p>In some special cases we also use cookies provided by trusted third parties. The following section
                details which third party cookies you might encounter through this site.</p><br>

            <ul>

                <li>
                    <p>This site uses Google Analytics which is one of the most widespread and trusted analytics
                        solution on the web for helping us to understand how you use the site and ways that we can
                        improve your experience. These cookies may track things such as how long you spend on the site
                        and the pages that you visit so we can continue to produce engaging content.</p>
                    <p>For more information on Google Analytics cookies, see the official Google Analytics page.</p>
                </li>
                <br>


                <li>
                    <p>From time to time we test new features and make subtle changes to the way that the site is
                        delivered. When we are still testing new features these cookies may be used to ensure that you
                        receive a consistent experience whilst on the site whilst ensuring we understand which
                        optimisations our users appreciate the most.</p>
                </li>
                <br>

            </ul>

            <p><strong>More Information</strong></p><br>

            <p>Hopefully that has clarified things for you and as was previously mentioned if there is something that
                you aren't sure whether you need or not it's usually safer to leave cookies enabled in case it does
                interact with one of the features you use on our site. This Cookies Policy was created with the help of
                the Generator of <a href="https://cookiepolicygenerator.com">GDPR Cookies Policy</a> and the <a
                        href="https://privacypolicygenerator.info">GDPR Privacy Policy</a>.</p><br>

            <p>However if you are still looking for more information then you can contact us through one of our
                preferred contact methods:</p><br>

            <ul>

                <li>By visiting this link: https://codingninja.eu</li>
            </ul>


        </div>
    </div>


@endsection