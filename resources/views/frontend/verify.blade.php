{{--Register layout and assign page variables--}}
@extends('layouts.codingninja', [
    'title' => 'Verify email address | ' . config('app.name'), //page title
    'pageID' => 'verify', //page body id for javascript
    'class' => 'profile' //page body class for css
])

{{--Main page content--}}
@section('content')

    <div style="min-height: calc(100vh - 10rem); display: flex; align-items: center; justify-content: center; ">

        @if(session()->has('info'))

            <p>{{ session()->get('info') }}</p>

            @else

            <p>Sorry, There is nothing here... Are you lost? If you want to go back click <a href="{{ url('/') }}">here</a></p>

        @endif

    </div>

@endsection