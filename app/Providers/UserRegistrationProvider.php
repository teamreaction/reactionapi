<?php

namespace CodingNinja\Providers;

use Illuminate\Support\ServiceProvider;
use CodingNinja\Events\UserRegistered;
use CodingNinja\User;

class UserRegistrationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::created(function ($user) {

            $token = $user->verificationToken()->create([
                'token' => bin2hex(random_bytes(32))
            ]);

            event(new UserRegistered($user));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
