<?php

namespace CodingNinja\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'CodingNinja\Events\Event' => [
            'CodingNinja\Listeners\EventListener',
        ],'CodingNinja\Events\UserRegistered' => [
            'CodingNinja\Listeners\SendVerificationEmail',
        ],
        'CodingNinja\Events\UserRequestedVerificationEmail' => [
            'CodingNinja\Listeners\SendVerificationEmail',
        ],
        'CodingNinja\Events\TeacherApproved' => [
            'CodingNinja\Listeners\SendApproveEmail',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
