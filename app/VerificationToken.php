<?php

namespace CodingNinja;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class VerificationToken extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token'
    ];

    /**
     * The relationship between user and VerificationToken
     *
     * @return Relation
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The name of the custom route
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'token';
    }
}
