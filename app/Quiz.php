<?php

namespace CodingNinja;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    const TYPES = array(
        0 => 'Simple Theory',
        1 => 'Multiple choice',
        2 => 'True or false',
    );

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'chapter_id', 'slug', 'description', 'theory', 'example'
    ];

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function types()
    {
        return self::TYPES;
    }

}
