<?php

namespace CodingNinja;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use phpDocumentor\Reflection\Types\Iterable_;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableInterface;

class User extends Authenticatable implements AuthenticatableInterface
{
    use Notifiable;

    const ADMIN_TYPE = 'admin';
    const DEFAULT_TYPE = 'default';
    const TEACHER_TYPE = 'teacher';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'type', 'verified', 'password', 'linkedin','github','about_me'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verified'
    ];

    /**
     * Check if user is admin
     *
     * @return bool
     */
    public function isAdmin()    {

        return $this->type === self::ADMIN_TYPE;
    }

    /**
     * Check if user is teacher
     *
     * @return bool
     */
    public function isTeacher()    {

        return $this->type === self::TEACHER_TYPE;
    }


    /**
     * The relation between verificationToken and User
     *
     * @return Relation
     */
    public function verificationToken()
    {
        return $this->hasOne(VerificationToken::class);
    }


    /**
     * Check if user has verified email
     *
     * @return boolean
     */
    public function hasVerifiedEmail()
    {
        return $this->verified;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function avatar()
    {
        return $this->hasOne(Avatar::class);
    }
}
