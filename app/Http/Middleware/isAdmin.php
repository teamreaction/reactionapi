<?php

namespace CodingNinja\Http\Middleware;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->isAdmin()) {
            $request->session()->flash('status', 'You are in admin zone'); //display message to show admin validation
            return $next($request);
        }

        return redirect('home');
    }
}
