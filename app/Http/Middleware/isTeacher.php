<?php

namespace CodingNinja\Http\Middleware;

use Closure;

class isTeacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->isTeacher() || auth()->user()->isAdmin()) {
            $request->session()->flash('status', 'You are teacher'); //display message to show admin validation
            return $next($request);
        }

        return redirect('home');
    }
}
