<?php

namespace CodingNinja\Http\Controllers\Auth;

use CodingNinja\User;
use CodingNinja\Http\Controllers\Controller;
use CodingNinja\Notifications\UserRegistrationMessage as UserRegistrationMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if($data['type'] == 'teacher') { //Teacher Validation

            return Validator::make($data, [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|confirmed|min:6',
                'linkedin' => 'required|string|max:255',
                'github' => 'required|string|max:255',
            ]);
        }

        return Validator::make($data, [ //Default User Validation
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \CodingNinja\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'first_name' => isset($data['first_name']) ? $data['first_name'] : null,
            'last_name' => isset($data['last_name']) ? $data['last_name'] : null,
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'type' => $data['type'] == 'teacher' ? User::TEACHER_TYPE : User::DEFAULT_TYPE,
            'linkedin' => isset($data['linkedin']) ? $data['linkedin'] : null,
            'github' => isset($data['github']) ? $data['github'] : null,
            'about_me' => null
        ]);


        return $user;
    }

    /**
     * Redirect User when created to verify email
     *
     * @param  Request $request
     * @return User $user
     */
    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();

        if($user['type'] == 'teacher') {

            return redirect('/verify')->with('info','Thank you! We will contact you as soon as we evaluate your application');
        }

        return redirect('/verify')->with('info','Please verify your email. Tip: you can check your inbox :)');
    }
}
