<?php

namespace CodingNinja\Http\Controllers\Auth;

use CodingNinja\VerificationToken;
use Illuminate\Http\Request;
use CodingNinja\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use CodingNinja\User;
use CodingNinja\Events\UserRequestedVerificationEmail;

class VerificationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Check for authentication token and then login user
     *
     * @param VerificationToken $token
     * @return User $user
     *
     * @throws \Exception
     */
    public function verify(VerificationToken $token)
    {
        $token->user()->update([
           'verified' => true
        ]);

        $token->delete();

        Auth::login($token->user()->firstOrFail());

        return redirect('/admin');
    }


    /**
     * Resend token if user's email is not verified
     *
     * @param Request $request
     * @return User $user
     *
     * @throws \Exception
     */
    public function resend(Request $request)
    {
        $user = User::where(['email' => $request->email])->firstOrFail();

        if($user->hasVerifiedEmail()) {

            return redirect('/home');
        }

        if($user['type'] == 'teacher') {

            return redirect('/login')->withInfo('We will evaluate your application soon...');
        }

        event(new UserRequestedVerificationEmail($user));

        return redirect('/login')->withInfo('Verification email resent. Please check your inbox');
    }
}
