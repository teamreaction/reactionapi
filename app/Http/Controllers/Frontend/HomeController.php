<?php

namespace CodingNinja\Http\Controllers\Frontend;


use CodingNinja\Course;
use CodingNinja\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.home');
    }


    /**
     * Show the application home page.
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chapters($slug)
    {
        $course = Course::with('chapters')
                       ->where(['slug' => $slug])
                        ->first();

        $chapters = $course->chapters()->get();

        return view('frontend.chapters', compact('course','chapters'));
    }
}
