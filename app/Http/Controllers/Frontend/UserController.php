<?php

namespace CodingNinja\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use CodingNinja\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use CodingNinja\User;
use Illuminate\Support\Facades\Hash;
use CodingNinja\Avatar;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = Auth::user();

        return view('frontend.profile', compact('user'));
    }

    /**
     * Update Email
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editemail(Request $request)
    {
        $user_id = $request->user()->id;

        $request->validate([
            'email' => 'required|email|max:255'
        ]);

        $user = User::find($user_id);
        $user['email'] = $request->get('email');

        $user->save();

        return back()->with('success', 'Your email was updated successfully!');
    }

    /**
     * Update Password
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editpassword(Request $request)
    {

        $user_id = $request->user()->id;

        $request->validate([
            'oldpassword' => 'required|max:255',
            'newpassword' => 'required|max:255|min:6'
        ]);

        $user = User::find($user_id);
        $oldpassword = $request->get('oldpassword');
        $newpassword = $request->get('newpassword');

        if(Hash::check($oldpassword, $user['password'])) {

            $user['password'] = Hash::make($newpassword);
            $user->save();

            return back()->with('success', 'Your password was updated successfully!');
        }

        return back()->with('fail', 'Invalid password');
    }

    /**
     * Update Avatar
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editavatar(Request $request)
    {
        $userid = $request->get('userID');
        $avatarfile = $request->get('avatar');

        $user = User::find($userid);

        if ($user->avatar()) {

            $user->avatar()->delete();
        }

        $avatar = new Avatar();
        $avatar['user_id'] = $userid;
        $avatar['avatar'] = 'avatar' . $userid . '.svg';
        $avatar->save();

        Storage::drive('local')->put('public/avatars/avatar' .  $userid . '.svg', $avatarfile);

        return response()->json(['status' => 200, 'avatar' => $avatar['avatar']]);
    }

}
