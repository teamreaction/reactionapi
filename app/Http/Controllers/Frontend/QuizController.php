<?php

namespace CodingNinja\Http\Controllers\Frontend;

use CodingNinja\Http\Controllers\Controller;
use CodingNinja\Quiz;
use CodingNinja\Chapter;

class QuizController extends Controller
{
    /**
     * Show a specific course with it's chapterss
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chapter = Chapter::with('quizzes', 'quizzes.answers')->find($id);
        if(is_null($chapter)) {
            return response()->json(null, 404);
        }

        return response()->json($chapter, 200);
    }

    /**
     * Show all courses.
     *
     * @param $id
     * @param $answer
     *
     * @return \Illuminate\Http\Response
     */
    public function check($id, $answer)
    {
        $quiz = Quiz::find($id);
        $currentAnswer = $quiz->answers->find($answer);
        $response = array();


        if(!isset($currentAnswer) || !isset($quiz)) {
            return response(null, '404');
        }

        if($currentAnswer['is_correct']) {

            $response['msg'] = 'Correct Answer';
        } else {

            $response['msg'] = 'Wrong Answer';
            $correctAnswer = $quiz->answers->where('is_correct', true)->first();
            $response['correct'] = 'The correct answer is: ' . $correctAnswer['answer'];
        }

        return response($response, '200');
    }
}
