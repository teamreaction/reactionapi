<?php

namespace CodingNinja\Http\Controllers;


use CodingNinja\Notifications\ContactMessage;
use CodingNinja\User;
use CodingNinja\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class LandingController extends Controller
{

    /**
     * Show the application landing page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.home');
    }

    /**
     * Show the application courses.
     *
     * @return \Illuminate\Http\Response
     */
    public function courses()
    {
        $courses = Course::orderBy('title')->get();

        return view('frontend.courses', compact('courses'));
    }

    /**
     * Show the application contact form
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('frontend.contact');
    }

    /**
     * Show the application verify notice
     *
     * @return \Illuminate\Http\Response
     */
    public function verify()
    {
        return view('frontend.verify');
    }

    /**
     * Show the application privacy policy
     *
     * @return \Illuminate\Http\Response
     */
    public function privacy()
    {
        return view('frontend.privacy');
    }

    /**
     * Show the application terms pf use
     *
     * @return \Illuminate\Http\Response
     */
    public function terms()
    {
        return view('frontend.terms');
    }


    /**
     * Send Message
     *
     * @param \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function email(Request $request)
    {
        $admins = User::where(['type' => 'admin'])->get();

        $request->validate([
            'email' => 'required|email|string|max:255',
            'name' => 'string|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string'
        ]);

        $form = new \stdClass();
        $form->email_address = $request->get('email');
        $form->name = $request->get('name');
        $form->subject = $request->get('subject');
        $form->message = $request->get('message');

        Notification::send($admins, new ContactMessage($form));

        return back()->with('success', 'Thank you for sending us. We will get to you asap!');
    }
}
