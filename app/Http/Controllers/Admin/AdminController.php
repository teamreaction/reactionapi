<?php

namespace CodingNinja\Http\Controllers\Admin;

use CodingNinja\Chapter;
use CodingNinja\User;
use Illuminate\Http\Request;
use CodingNinja\Http\Controllers\Controller;
use CodingNinja\Course;
use CodingNinja\Quiz;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('is_teacher');
    }

    /**
     * Show the application admin page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pendingteachers = User::where(['type' => 'teacher'])->where(['verified' => false])->get();

        return view('admin.admin', compact('pendingteachers'));
    }

    /**
     * Fetch teacher pending requests.
     *
     * @return \Illuminate\Http\Response
     */
    public function teacherpending()
    {
        $pendingteachers = User::where(['type' => 'teacher'])->where(['verified' => false])->get();

        $content = '<tr id="pendingRequests"></tr>';

        foreach ($pendingteachers as $teacher) {

            $content = '<tr id="pendingRequests">';
            $content .= '<td class="pt-3-half" contenteditable="false"><a href="/users/'. $teacher['id'] .'/edit">' . $teacher['username'] . '</a></td>';
            $content .= '<td class="pt-3-half" contenteditable="false">' . $teacher['email'] . '</td>';
            $content .= '<td class="pt-3-half" contenteditable="false">' . $teacher['verified'] . '</td>';
            $content .= '</tr>';
        }

        return response(['status' => 200, 'content' => $content]);
    }


    /**
     * Fetch application current content.
     *
     * @return \Illuminate\Http\Response
     */
    public function currentcontent()
    {

        //Get content count
        $courses = Course::count();
        $chapters = Chapter::count();
        $quizzes = Quiz::count();

        return response(['status' => 200, 'courses' => $courses, 'chapters' => $chapters, 'quizzes' => $quizzes]);
    }

}
