<?php

namespace CodingNinja\Http\Controllers\Admin;

use CodingNinja\Chapter;
use CodingNinja\Course;
use Illuminate\Http\Request;
use CodingNinja\Http\Controllers\Controller;

class ChapterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('is_teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chapters = Chapter::all();
        return view('admin.chapters.index', compact('chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.chapters.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'course_id' => 'required',
            'content' => 'required'
        ]);

        $chapter = new Chapter();
        $chapter['title'] = $request->get('title');
        $chapter['course_id'] = $request->get('course_id');
        $chapter['slug'] = str_slug($chapter['title'], '-');
        $chapter['content'] = $request->get('content');
        $chapter->save();

        return redirect('/chapters')->with('success', 'Chapter has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chapter = Chapter::find($id);
        $courses = Course::all();
        return view('admin.chapters.edit', compact('chapter', 'courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string',
            'course_id' => 'required',
            'content' => 'required'
        ]);

        $chapter = Chapter::find($id);
        $chapter['title'] = $request->get('title');
        $chapter['course_id'] = $request->get('course_id');
        $chapter['slug'] = str_slug($chapter['title'], '-');
        $chapter['content'] = $request->get('content');
        $chapter->save();

        return redirect('/chapters')->with('success', 'Chapter has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chapter = Chapter::find($id);
        $chapter->delete();

        return redirect('/chapters')->with('success', 'Chapter has been deleted successfully');
    }
}
