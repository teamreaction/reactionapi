<?php

namespace CodingNinja\Http\Controllers\Admin;


use CodingNinja\Events\TeacherApproved;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use CodingNinja\Http\Controllers\Controller;
use CodingNinja\User;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('is_admin');
    }


    /**
     * Show the application users page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $current_user_id = Auth::user()->id;
        $users = User::where('id', '<>', $current_user_id)->paginate(15); //return all users without current user


        return view('admin.users.index', compact('users'));
    }


    /**
     * Show the application user profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function editprofile(Request $request)
    {
        $user_id = $request->user()->id;

        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'facebook' => 'max:255',
            'instagram' => 'max:255',
            'github' => 'max:255'
        ]);

        $user = User::find($user_id);
        $user['first_name'] = $request->get('first_name');
        $user['last_name'] = $request->get('last_name');
        $user['email'] = $request->get('email');
        $user['facebook'] = $request->get('facebook');
        $user['instagram'] = $request->get('instagram');
        $user['github'] = $request->get('github');
        $user['about_me'] = $request->get('about_me');

        $user->save();

        return back()->with('success', 'Your profile has been updated successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'type' => 'required|string'
        ]);

        $user = User::find($id);
        $user['first_name'] = $request->get('first_name');
        $user['last_name'] = $request->get('last_name');
        $user['username'] = $request->get('username');
        $user['email'] = $request->get('email');
        $user['type'] = $request->get('type');


        if($user['type'] === 'teacher') {

            event(new TeacherApproved($user));
            $user['verified'] = true;
        }

        $user->save();


        return redirect('/users')->with('success', 'User has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/users')->with('success', 'User has been deleted successfully');
    }
}
