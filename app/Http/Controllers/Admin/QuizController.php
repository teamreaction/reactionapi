<?php

namespace CodingNinja\Http\Controllers\Admin;

use CodingNinja\Answer;
use CodingNinja\Chapter;
use CodingNinja\Http\Controllers\Controller;
use CodingNinja\Http\Requests\AdminStoreQuiz as StoreQuiz;
use CodingNinja\Quiz;

class QuizController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('is_teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quizzes = Quiz::all();
        return view('admin.quizzes.index', compact('quizzes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $chapters = Chapter::all();
        return view('admin.quizzes.create', compact('chapters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreQuiz $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuiz $request)
    {
        //Get Quiz Type
        $type_id = $request->get('type_id');

        if ($type_id == array_search('Multiple choice', Quiz::TYPES)){

            $request->validate([
                'title' => 'required|string|max:255|unique:quizzes',
                'chapter_id' => 'required|integer',
                'description' => 'required|string',
                'answer.*' => 'required|string|max:255',
                'correct' => 'required',
            ]);

            //Save quiz at first
            $quiz = new Quiz();
            $quiz['title'] = $request->get('title');
            $quiz['chapter_id'] = $request->get('chapter_id');
            $quiz['slug'] = str_slug($quiz['title'], '-');
            $quiz['type_id'] = $request->get('type_id');
            $quiz['description'] = $request->get('description');
            $quiz->save();

            //Save answers then
            foreach ($request->get('answer') as $key => $answerContent) {
                $answer = new Answer;
                $answer['answer'] = $answerContent;
                $answer['is_correct'] = $key == $request->get('correct') ? true : false; //check for correct answer
                $answer['quiz_id'] = $quiz['id'];
                $answer->save();
            }
        }

        if ($type_id == array_search('Simple Theory', Quiz::TYPES)){

            $request->validate([
                'title' => 'required|string|max:255|unique:quizzes',
                'chapter_id' => 'required|integer',
                'description' => 'required|string',
                'theory' => 'required|string',
                'example' => 'string'
            ]);

            //Save quiz at first
            $quiz = new Quiz();
            $quiz['title'] = $request->get('title');
            $quiz['chapter_id'] = $request->get('chapter_id');
            $quiz['slug'] = str_slug($quiz['title'], '-');
            $quiz['type_id'] = $request->get('type_id');
            $quiz['description'] = $request->get('description');
            $quiz['theory'] = $request->get('theory');
            $quiz['example'] = $request->get('example') ? $request->get('example') : null;
            $quiz->save();
        }

        if ($type_id == array_search('True or false', Quiz::TYPES)) {

            $request->validate([
                'title' => 'required|string|max:255|unique:quizzes',
                'chapter_id' => 'required|integer',
                'description' => 'required|string',
                'correctAnswer' => 'required'
            ]);

            //Save quiz at first
            $quiz = new Quiz();
            $quiz['title'] = $request->get('title');
            $quiz['chapter_id'] = $request->get('chapter_id');
            $quiz['slug'] = str_slug($quiz['title'], '-');
            $quiz['type_id'] = $request->get('type_id');
            $quiz['description'] = $request->get('description');

            $quiz->save();

            //Save true
            $answer = new Answer;
            $answer['answer'] = 'true';
            $answer['is_correct'] = $answer['answer'] == $request->get('correctAnswer') ? true : false; //check for correct answer
            $answer['quiz_id'] = $quiz['id'];
            $answer->save();

            //Save false
            $answer = new Answer;
            $answer['answer'] = 'false';
            $answer['is_correct'] = $answer['answer'] == $request->get('correctAnswer') ? true : false; //check for correct answer
            $answer['quiz_id'] = $quiz['id'];
            $answer->save();
        }

        return redirect('/quizzes')->with('success','A new quiz has been added successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quiz = Quiz::find($id);
        $chapters = Chapter::all();
        return view('admin.quizzes.edit', compact('quiz','chapters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreQuiz $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreQuiz $request, $id)
    {
        $quiz = Quiz::find($id);

        $type_id = $quiz['type_id']; //get type

        $quiz->answers()->delete(); //remove answers first

        if ($type_id === 1){

            $request->validate([
                'title' => 'required|string|max:255|unique:quizzes',
                'chapter_id' => 'required|integer',
                'description' => 'required|string',
                'answer.*' => 'required|string|max:255',
                'correct' => 'required',
            ]);

            //Save quiz at first
            $quiz['title'] = $request->get('title');
            $quiz['chapter_id'] = $request->get('chapter_id');
            $quiz['slug'] = str_slug($quiz['title'], '-');
            $quiz['type_id'] = $request->get('type_id');
            $quiz['description'] = $request->get('description');
            $quiz->save();

            //Save answers then
            foreach ($request->get('answer') as $key => $answerContent) {
                $answer = new Answer;
                $answer['answer'] = $answerContent;
                $answer['is_correct'] = $key == $request->get('correct') ? true : false; //check for correct answer
                $answer['quiz_id'] = $quiz['id'];
                $answer->save();
            }
        }

        if ($type_id === 0){

            $request->validate([
                'title' => 'required|string|max:255|unique:quizzes',
                'chapter_id' => 'required|integer',
                'description' => 'required|string',
                'theory' => 'required|string',
                'example' => 'string'
            ]);

            //Save quiz at first
            $quiz['title'] = $request->get('title');
            $quiz['chapter_id'] = $request->get('chapter_id');
            $quiz['slug'] = str_slug($quiz['title'], '-');
            $quiz['type_id'] = $request->get('type_id');
            $quiz['description'] = $request->get('description');
            $quiz['theory'] = $request->get('theory');
            $quiz['example'] = $request->get('example') ? $request->get('example') : null;
            $quiz->save();
        }

        if ($type_id === 2) {

            $request->validate([
                'title' => 'required|string|max:255|unique:quizzes',
                'chapter_id' => 'required|integer',
                'description' => 'required|string',
                'correctAnswer' => 'required'
            ]);

            //Save quiz at first
            $quiz['title'] = $request->get('title');
            $quiz['chapter_id'] = $request->get('chapter_id');
            $quiz['slug'] = str_slug($quiz['title'], '-');
            $quiz['type_id'] = $request->get('type_id');
            $quiz['description'] = $request->get('description');

            $quiz->save();

            //Save true
            $answer = new Answer;
            $answer['answer'] = 'true';
            $answer['is_correct'] = $answer['answer'] == $request->get('correctAnswer') ? true : false; //check for correct answer
            $answer['quiz_id'] = $quiz['id'];
            $answer->save();

            //Save false
            $answer = new Answer;
            $answer['answer'] = 'false';
            $answer['is_correct'] = $answer['answer'] == $request->get('correctAnswer') ? true : false; //check for correct answer
            $answer['quiz_id'] = $quiz['id'];
            $answer->save();
        }


        return redirect('/quizzes')->with('success','The quiz has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = Quiz::find($id);
        $quiz->answers()->delete();
        $quiz->delete();

        return redirect('/quizzes')->with('success', 'The quiz has been deleted successfully');
    }
}
