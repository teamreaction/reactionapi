<?php

namespace CodingNinja\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AdminStoreQuiz extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required',
            'chapter_id' => 'required|integer',
            'answer1.*' => 'required|distinct',
            'answer2.*' => 'required|distinct',
            'answer3.*' => 'required|distinct',
            'answer4.*' => 'required|distinct'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'answer1.*.required' => 'The first answer is required',
            'answer2.*.required' => 'The second answer is required',
            'answer3.*.required' => 'The third answer is required',
            'answer4.*.required' => 'The fourth answer is required',
        ];
    }
}
