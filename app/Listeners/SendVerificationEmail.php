<?php

namespace CodingNinja\Listeners;

use CodingNinja\Mail\sendApplicationPending;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use CodingNinja\Mail\sendVerificationToken;
use Illuminate\Support\Facades\Notification;
use CodingNinja\User;
use CodingNinja\Notifications\ContactMessage;

class SendVerificationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if($event->user['type'] == 'teacher') {

            Mail::to($event->user)->send(new sendApplicationPending());

            $admins = User::where(['type' => 'admin'])->get();

            $form = new \stdClass();
            $form->email_address = $event->user['email'];
            $form->name = $event->user['first_name'] . ' ' . $event->user['last_name'];
            $form->subject = 'New teacher registrations';
            $form->message = 'New teacher has been applied to our application. You can check his LinkedIn <a herf="' . $event->user['linkedin'] . '">here</a> ' . 'and his Github <a href="' . $event->user['github'] . '">here</a>';

            Notification::send($admins, new ContactMessage($form));

        } else {

            Mail::to($event->user)->send(new sendVerificationToken($event->user->verificationToken));
        }

    }
}
