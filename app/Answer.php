<?php

namespace CodingNinja;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    const trueorfalse = array(
        'true',
        'false'
    );

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }

}
