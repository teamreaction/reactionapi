<?php

namespace CodingNinja;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Course extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'created_at', 'updated_at'
    ];

    /**
     * The relation with chapters
     *
     * @return Relation
     */
    public function chapters()
    {
        return $this->hasMany(Chapter::class);
    }


    /**
     * Check if course has active chapters
     *
     * @return string
     */
    public function checkStatus()
    {
        return $this->hasMany(Chapter::class)->count() > 0 ? 'active' : 'comingSoon';
    }
}
