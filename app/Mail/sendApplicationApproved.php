<?php

namespace CodingNinja\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use CodingNinja\User;

class sendApplicationApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $user;


    /**
     * sendApplicationApproved constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('You became a teacher!')
            ->view('email.sendApproved');
    }
}
