<?php

namespace CodingNinja\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactMessage extends Notification
{
    use Queueable;
    protected $name;
    protected $email_address;
    protected $subject;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @param \stdClass $form
     *
     * @return void
     */
    public function __construct(\stdClass $form)
    {
        $this->name = $form->name;
        $this->email_address = $form->email_address;
        $this->subject = $form->subject;
        $this->message = $form->message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = sprintf('%s: You \'ve got a new message from %s', config('app.name'), $this->name);
        $greeting = sprintf('Hello %s', $notifiable->username);
        $salutation = sprintf('Yours Faithfully, %s',  $this->name);

        return (new MailMessage)
                    ->from($this->email_address)
                    ->subject($subject)
                    ->greeting($greeting)
                    ->line($this->message)
                    ->salutation($salutation);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
