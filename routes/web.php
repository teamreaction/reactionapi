<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Landing Page
 */
Route::get('/', 'LandingController@index')->name('landing');

/*
 * Contact Form
 */
Route::get('/contact', 'LandingController@contact')->name('contact');
Route::post('/email', 'LandingController@email')->name('email');

/*
 * Terms of use and privacy policy
 */
Route::get('/privacy-policy', 'LandingController@privacy')->name('privacy');
Route::get('/terms-of-use', 'LandingController@terms')->name('terms');

/*
 * User login, register, reset password and forgot password routes
 */
Auth::routes();

/*
 * Authenticate email routes
 */
Route::get('/verify', 'LandingController@verify')->name('verify');
Route::get('/verify/token/{token}', 'Auth\VerificationController@verify')->name('auth.verify');
Route::get('/verify/resend/', 'Auth\VerificationController@resend')->name('auth.verify.resend');

/*
 * Guest section after successful login or registration
 */
Route::get('/home', 'Frontend\HomeController@index')->name('home');

/*
 * Guest section after successful login or registration display courses
 */
Route::get('/library/courses', 'LandingController@courses')->name('frontend.courses');

/*
 * Guest section after successful login or registration display courses
 */
Route::get('/library/{course}/chapters', 'Frontend\HomeController@chapters')->name('frontend.courses.chapters');

/*
 * User profile section
 */
Route::get('/profile', 'Frontend\UserController@profile')->name('frontend.profile');

/*
 * Edit user email form
 */
Route::post('/editemail', 'Frontend\UserController@editemail')->name('frontend.editemail');

/*
 * Edit user email form
 */
Route::post('/editpassword', 'Frontend\UserController@editpassword')->name('frontend.editpassword');

/*
 * Edit user avatar
 */
Route::post('/editavatar', 'Frontend\UserController@editavatar')->name('frontend.editvatar');


/*
 * Edit profile section
 */
Route::patch('/editprofile', 'Admin\UserController@editprofile')->name('editprofile');

/*
 * Admin and Teacher section
 */
Route::middleware('is_teacher')->group(function() {

    //Admin main page
    Route::get('/admin', 'Admin\AdminController@index')->name('admin');

    //Admin courses page control
    Route::resource('courses', 'Admin\CourseController');

    //Admin chapters page control
    Route::resource('chapters', 'Admin\ChapterController');

    //Admin quizzes page control
    Route::resource('quizzes', 'Admin\QuizController');

    //Get current content
    Route::post('/currentcontent', 'Admin\AdminController@currentcontent')->name('currentcontent');
});

/*
 * Only Admin section
 */
Route::middleware('is_admin')->group(function() {

    //Admin users page control
    Route::resource('users','Admin\UserController')->except([
        'create', 'store', 'show'
    ]);

    Route::post('/teacherpending', 'Admin\AdminController@teacherpending')->name('teacherpending');

});

