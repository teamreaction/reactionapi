let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

//Custom Fonts
// mix.copyDirectory('resources/assets/fonts', 'public/fonts');

//Custom Admin CSS && JS
mix.sass('resources/assets/sass/admin/app.scss', 'public/css/admin')
    .js('resources/assets/js/admin/app.js', 'public/js/admin')

//Custom Frontend CSS && JS
mix.sass('resources/assets/sass/frontend/style.sass', 'public/css/frontend')
    .options({ processCssUrls: false })
    .js('resources/assets/js/frontend/script.js', 'public/js/frontend');
