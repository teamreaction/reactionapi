<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->integer('chapter_id')->unsigned()->index();
            $table->foreign('chapter_id')->references('id')->on('chapters');
            $table->integer('type_id')->index();
            $table->string('slug');
            $table->text('description')->nullable();
            $table->text('theory')->nullable();
            $table->text('example')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
